import json
from markupsafe import Markup
from logs.models import StationAction
from templating import register_filter
from difflib import unified_diff


def action2icon(action):
    icons = {
        StationAction: {
            'create': 'icon-plus-sign',
            'edit': 'icon-edit',
            'delete': 'icon-remove-sign',
            'read': 'icon-info-sign',
        },
    }[type(action)]
    return icons[action.typ]


def action2label(action):
   labels = {
        StationAction: {
            'create': 'label-success',
            'edit': 'label-warning',
            'delete': 'label-important',
            'read': 'label-info',
        },
   }[type(action)]
   return labels[action.typ]


def station_description_diff(diff):
    lines = []
    if diff:
        result = json.loads(diff)
        for line in result:
            if line.startswith('+'):
                lines.append('<li class="diff-plus">%(line)s</li>' % {'line': line})
            elif line.startswith('-'):
                lines.append('<li class="diff-minus">%(line)s</li>' % {'line': line})
            else:
                lines.append('<li class="diff-none">%(line)s</li>' % {'line': line})
    return Markup('<ul>%s</ul>' % '\n'.join(lines))


def register_filters(env):
    register_filter(action2label, name='admin.action2label', env=env)
    register_filter(action2label, name='admin.action2icon', env=env)
    register_filter(station_description_diff, name='admin.description_diff', env=env)