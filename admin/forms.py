from __future__ import absolute_import
from django import forms
from django.db.models import Q
from django.db.models.query import EmptyQuerySet
from django.utils.translation import ugettext_lazy as _
from stations.models import RadioStation, Genre
from streams.models import Stream
from transmitters.models import REGIONS, City, TransmitterLocation

from utils.forms import SelfAwareFormMixin, QueryField


class StationFilterForm(SelfAwareFormMixin):

    query = QueryField(label='&nbsp;', required=False,
                       widget=forms.TextInput(attrs={'placeholder': _('station name, city')}))
    sub_types = forms.MultipleChoiceField(label='&nbsp;', choices=[('radiostation', _('radio station')),
                                                                   ('tvstation', _('tv station'))],
                                          widget=forms.CheckboxSelectMultiple(), required=False)
    genres = forms.ModelMultipleChoiceField(label='&nbsp;', queryset=Genre.objects.all(),
                                            widget=forms.CheckboxSelectMultiple(), required=False)
    recommended = forms.BooleanField(label=_('Recommended'), required=False)


    def __init__(self, stations, *args, **kwargs):
        self.stations = stations
        super(StationFilterForm, self).__init__(*args, **kwargs)

    def search(self):
        stations = self.stations
        parts = self.cleaned_data.get('query')
        if parts:
            query = Q()
            for part in parts:
                query &= (Q(name__icontains=part) |
                          Q(city__icontains=part) |
                          Q(country__icontains=part))
            stations = stations.filter(query)
        sub_types = self.cleaned_data['sub_types']
        if sub_types:
            query = Q()
            for typ in sub_types:
                query |= Q(subtype_attr__icontains=typ)
            stations = stations.filter(query)
        genres = self.cleaned_data['genres']
        if genres:
            query = Q()
            for genre in genres:
                query |= Q(radiostation__genres=genre)
            stations = stations.filter(query)
        if self.cleaned_data.get('recommended'):
            stations = stations.filter(recommended=True)
        return stations.distinct()


class TransmitterFilterForm(SelfAwareFormMixin):

    query = QueryField(label='&nbsp;', required=False,
                       widget=forms.TextInput(attrs={'placeholder': _('station name, location')}))
    regions = forms.MultipleChoiceField(label='&nbsp;', choices=[(k, v[0]) for k, v in REGIONS.items()],
                                        widget=forms.CheckboxSelectMultiple(), required=False)

    def __init__(self, transmitters, *args, **kwargs):
        self.transmitters = transmitters
        super(TransmitterFilterForm, self).__init__(*args, **kwargs)
        #self.fields['regions']._choices =

    def search(self):
        transmitters = self.transmitters.select_related('radiotransmitter',
                                                        'tvtransmitter')
        parts = self.cleaned_data.get('query')
        if parts:
            query = Q()
            for part in parts:
                query &= (Q(radiotransmitter__station__name__icontains=part) |
                          Q(tvtransmitter__station__name__icontains=part) |
                          Q(location__location__icontains=part))
            transmitters = transmitters.filter(query)
        regions = self.cleaned_data.get('regions')
        if regions:
            query = Q()
            for region in regions:
                query |= Q(location__region=region)
            transmitters = transmitters.filter(query)
        return transmitters.distinct()

class UseExistingCityChoiceForm(forms.Form):

    use_existing_city = forms.BooleanField(label=_('Use existing city'), required=False)


class UseExistingTransmitterLocationChoiceForm(forms.Form):

    use_existing_location = forms.BooleanField(label=_('Use existing location'), required=False)


class CityChoiceForm(forms.Form):

    city = forms.ModelChoiceField(label=_('City'), queryset=EmptyQuerySet())

    def __init__(self, transmitter, *args, **kwargs):
        self.transmitter = transmitter.get_subtype_instance()
        super(CityChoiceForm, self).__init__(*args, **kwargs)
        self.fields['city'].queryset = City.objects.exclude(transmitters=transmitter)


class CityForm(forms.ModelForm):

    class Meta:
        model = City
        fields = ['name', 'country', 'region', 'lat', 'lng']


class TransmitterLocationChoiceForm(forms.Form):

    location = forms.ModelChoiceField(label=_('Location'), queryset=TransmitterLocation.objects.all())


class TransmitterLocationForm(forms.ModelForm):

    class Meta:
        model = TransmitterLocation


class BroadcastingFormBase(forms.Form):

    quality = forms.ChoiceField(label=_('Quality'),
                                choices=[('1', _('very low')), ('2', _('low')),
                                         ('3', _('decent')), ('4', _('good')),
                                         ('5', _('very good'))],
                                widget=forms.RadioSelect())

    def __init__(self, transmitter, *args, **kwargs):
        self.transmitter = transmitter.get_subtype_instance()
        super(BroadcastingFormBase, self).__init__(*args, **kwargs)


class TransmitterTypeChoiceForm(forms.Form):

    typ = forms.ChoiceField(label=_('Type'), choices=[('FM', _('FM transmitter')),
                                                      ('DAB', _('DAB transmitter'))])


class GoogleAdsForm(forms.Form):

    client_id = forms.CharField(label=_('Client id'), max_length=20)


class GoogleAdEditForm(forms.Form):

    slot = forms.CharField(label=_('Slot'), max_length=20)
    width = forms.CharField(label=_('Width'), max_length=5)
    height = forms.CharField(label=_('Height'), max_length=5)

    def __init__(self, ad, *args, **kwargs):
        self.ad = ad
        super(GoogleAdEditForm, self).__init__(*args, **kwargs)
        self.initial['slot'] = ad['slot']
        self.initial['width'] = ad['width']
        self.initial['height'] = ad['height']


class CityFilterForm(forms.Form):

    query = QueryField(label='&nbsp;', required=False,
                       widget=forms.TextInput(attrs={'placeholder': _('city name')}))
    regions = forms.MultipleChoiceField(label='&nbsp;', choices=[(k, v[0]) for k, v in REGIONS.items()],
                                        widget=forms.CheckboxSelectMultiple(), required=False)


    def __init__(self, cities, *args, **kwargs):
        self.cities = cities
        super(CityFilterForm, self).__init__(*args, **kwargs)

    def search(self):
        cities = self.cities
        parts = self.cleaned_data.get('query')
        if parts:
            query = Q()
            for part in parts:
                query &= (Q(name__icontains=part))
            cities = cities.filter(query)
        regions = self.cleaned_data.get('regions')
        if regions:
            query = Q()
            for region in regions:
                query |= Q(region=region)
            cities = cities.filter(query)
        return cities.distinct()


class UserFilterForm(forms.Form):

    query = QueryField(label='&nbsp;', required=False,
                       widget=forms.TextInput(attrs={'placeholder': _('email, first or last name')}))
    is_staff = forms.BooleanField(label=_('is staff'), required=False)
    is_admin = forms.BooleanField(label=_('is admin'), required=False)


    def __init__(self, users, *args, **kwargs):
        self.users = users
        super(UserFilterForm, self).__init__(*args, **kwargs)

    def search(self):
        users = self.users
        parts = self.cleaned_data.get('query')
        if parts:
            query = Q()
            for part in parts:
                query &= (Q(email__icontains=part) |
                          Q(first_name__icontains=part) |
                          Q(last_name__icontains=part))
            users = users.filter(query)
        is_admin = self.cleaned_data.get('is_admin')
        is_staff = self.cleaned_data.get('is_staff')
        if is_staff:
            users = users.filter(is_staff=is_staff)
        if is_admin:
            users = users.filter(is_admin=is_admin)
        return users.distinct()


class StreamAddForm(forms.ModelForm):

    def clean(self):
        cleaned_data = super(StreamAddForm, self).clean()
        url = cleaned_data.get('url')
        if url and  Stream.objects.filter(station=self.instance.station, url=url).exists():
            raise forms.ValidationError(_('Stream url already exists in station.'))
        return cleaned_data
