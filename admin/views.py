#-*- coding: utf-8 -*-
from __future__ import absolute_import

from . import utils
from django.conf.urls import url, patterns
from django.contrib import messages
from django.db.models import Q
from django.forms.models import inlineformset_factory, modelform_factory
from django.http import Http404, HttpResponse
from django.shortcuts import redirect, get_object_or_404
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext
from . import forms
import simplejson
from logs.models import Action
from logs.signals import station_add, station_edit, station_delete
from stations.forms import StationFormBase, StationTypeSelectForm, StationImageForm

from stations.models import Station, RadioStation, TVStation, StationImage
from streams.models import Stream, VideoStream, AudioStream
from templating import render_to_response
from transmitters import Broadcast
from transmitters.models import Transmitter, FMTransmitter, DABTransmitter, TVTransmitter, TransmitterLocation, City, CityTransmitter
from users.decorators import admin_required, staff_required, admin_view
from users.forms import PasswordForm
from users.models import User
from utils.forms import ConfirmationForm


class StationAdminApp(utils.ModelAdminApp):

    def __init__(self):
        super(StationAdminApp, self).__init__(model=Station)

    @method_decorator(staff_required)
    def index(self, request):
        stations = Station.objects.all()
        filter_form = forms.StationFilterForm(data=request.GET, stations=stations)
        if filter_form.is_valid():
            stations = filter_form.search()
        context = self.get_context_data(request, stations=stations, filter_form=filter_form)
        templates = self._get_templates('index')
        return render_to_response(templates, request=request, context=context)

    @method_decorator(admin_view(require_permissions=('stations_management',)))
    def station_type_select(self, request):
        form = StationTypeSelectForm(data=request.POST or None)
        if form.is_valid():
            typ = form.cleaned_data['typ']
            redirect_to = self.reverse('add', args=[typ])
            return redirect(redirect_to)
        cancel_url = self.reverse('index')
        context = self.get_context_data(request, form=form, cancel_url=cancel_url)
        templates = self._get_templates('type_select')
        return render_to_response(templates, request=request, context=context)

    @method_decorator(admin_view(require_permissions=('stations_management',)))
    def add(self, request, typ):
        StationModel = {'radio': RadioStation, 'tv': TVStation}[typ]
        station = StationModel(added_by=request.user)
        StationForm = modelform_factory(StationModel, exclude=['logo', 'added_by'])
        station_form = StationForm(data=request.POST or None,
                                   instance=station)
        desc1 = station.description
        for field in station_form.fields:
            print field
        if station_form.is_valid():
            station = station_form.save()
            station_add.send(station=station, author=request.user, desc1=desc1,
                             desc2=station_form.cleaned_data['description'], sender=Station)
            messages.success(request, ugettext(u'Station created successfully.'))
            redirect_to = self.reverse('details', args=[station.pk])
            return redirect(redirect_to)
        cancel_url = self.reverse('index')
        context = self.get_context_data(request, cancel_url=cancel_url,
                                        station_form=station_form)
        templates = self._get_templates('add')
        return render_to_response(templates, request=request, context=context)

    @method_decorator(admin_view(require_permissions=('stations_management',)))
    def edit(self, request, pk):
        if request.is_ajax():
            if 'userId' in request.GET:
                try:
                    user = User.objects.get(pk=request.GET.get('userId'))
                    return HttpResponse(simplejson.dumps({'username': user.full_name, 'id': user.pk}))
                except User.DoesNotExist:
                    return HttpResponse(simplejson.dumps({}))
            # get queryset of possible owners
            parts = request.GET.get('q', '').split()
            users = User.objects.all()
            if parts:
                query = Q()
                for part in parts:
                    query &= (Q(first_name__icontains=part) |
                              Q(last_name__icontains=part) |
                              Q(email__icontains=part))
                users = users.filter(query).distinct()
                users = [{'username': u.full_name, 'id': u.pk} for u in users]
                return HttpResponse(simplejson.dumps(users))
            return HttpResponse(simplejson.dumps([]))
        else:
            station = get_object_or_404(Station, pk=pk)
            station = station.get_subtype_instance()
            StationForm = modelform_factory(type(station), form=StationFormBase)
            station_form = StationForm(data=request.POST or None,
                                       instance=station)
            desc1 = station.description
            if station_form.is_valid():
                station = station_form.save()
                station_edit.send(station=station, author=request.user, desc1=desc1,
                                  desc2=station_form.cleaned_data['description'], sender=Station)
                messages.success(request, ugettext(u'Station changed successfully.'))
                redirect_to = self.reverse('details', args=[station.pk])
                return redirect(redirect_to)
            cancel_url = self.reverse('details', args=[station.pk])
            context = self.get_context_data(request, cancel_url=cancel_url,
                                            station_form=station_form, station=station)
            templates = self._get_templates('edit')
            return render_to_response(templates, request=request, context=context)

    @method_decorator(admin_view(require_permissions=('stations_management',)))
    def details(self, request, pk):
        station = get_object_or_404(Station, pk=pk)
        station = station.get_subtype_instance()
        streams = station.streams.all()
        context = self.get_context_data(request, station=station, streams=streams)
        templates = self._get_templates('details')
        return render_to_response(templates, request=request, context=context)

    @method_decorator(admin_view(require_permissions=('stations_management',)))
    def delete(self, request, pk):
        station = get_object_or_404(Station, pk=pk)
        form = ConfirmationForm(data=request.POST or None)
        if form.is_valid():
            station.delete()
            station_delete.send(station=station, author=request.user, sender=Station)
            messages.success(request, ugettext(u'Station deleted successfully.'))
            redirect_to = self.reverse('index')
            return redirect(redirect_to)
        cancel_url = self.reverse('details', args=[station.pk])
        context = self.get_context_data(request, station=station, form=form,
                                        cancel_url=cancel_url)
        templates = self._get_templates('delete')
        return render_to_response(templates, request=request, context=context)

    @method_decorator(admin_view(require_permissions=('stations_management',)))
    def station_image_add(self, request, station_pk):
        station = get_object_or_404(Station, pk=station_pk)
        image = StationImage(station=station)
        Form = modelform_factory(StationImage, form=StationImageForm, exclude=['station'])
        form = Form(data=request.POST or None, files=request.FILES or None, instance=image)
        redirect_to = self.reverse('details', args=[station.pk])
        if form.is_valid():
            image = form.save()
            if form.cleaned_data.get('set_as_logo', False):
                station.logo = image
            station.save()
            messages.success(request, ugettext(u'Station image added successfully.'))
            return redirect(redirect_to)
        cancel_url = redirect_to
        context = self.get_context_data(request, station=station, image=image,
                                        form=form, cancel_url=cancel_url)
        templates = self._get_templates('image_add')
        return render_to_response(templates, request=request, context=context)


    @method_decorator(admin_view(require_permissions=('stations_management',)))
    def station_image_edit(self, request, station_pk, pk):
        station = get_object_or_404(Station, pk=station_pk)
        image = get_object_or_404(StationImage, pk=pk, station=station)
        form = StationImageForm(data=request.POST or None, files=request.FILES or None, instance=image)
        print form
        redirect_to = self.reverse('details', args=[station.pk])
        if form.is_valid():
            image = form.save()
            if form.cleaned_data.get('set_as_logo', False):
                station.logo = image
            station.save()
            messages.success(request, ugettext(u'Station image changed successfully.'))
            return redirect(redirect_to)
        cancel_url = redirect_to
        context = self.get_context_data(request, station=station, image=image,
                                        form=form, cancel_url=cancel_url)
        templates = self._get_templates('image_edit')
        return render_to_response(templates, request=request, context=context)

    @method_decorator(admin_view(require_permissions=('stations_management',)))
    def station_image_delete(self, request, station_pk, pk):
        station = get_object_or_404(Station, pk=station_pk)
        image = get_object_or_404(StationImage, pk=pk, station=station)
        form = ConfirmationForm(data=request.POST or None)
        redirect_to = self.reverse('details', args=[station.pk])
        if form.is_valid():
            image.thumbnails.all().delete()
            image.delete()
            messages.success(request, ugettext(u'Station\'s image deleted successfully.'))
            return redirect(redirect_to)
        cancel_url = redirect_to
        context = self.get_context_data(request, station=station, image=image,
                                        cancel_url=cancel_url, form=form)
        templates = self._get_templates('image_delete')
        return render_to_response(templates, request=request, context=context)

    @method_decorator(admin_view(require_permissions=('transmitters_management',)))
    def station_transmitter_type_select(self, request, station_pk):
        station = get_object_or_404(Station, pk=station_pk).get_subtype_instance()
        if isinstance(station, TVStation):
            messages.error(request, ugettext(u'TV stations not supported yet.'))
            return redirect(self.reverse('details', args=[station.pk]))
        form = forms.TransmitterTypeChoiceForm(data=request.POST or None)
        if form.is_valid():
            typ = form.cleaned_data['typ']
            return redirect(self.reverse('radio-transmitter-add', args=[station.pk, typ]))
        cancel_url = self.reverse('details', args=[station.pk])
        context = self.get_context_data(request, form=form, cancel_url=cancel_url, station=station)
        templates = self._get_templates('transmitter_type_select')
        return render_to_response(templates, request=request, context=context)

    @method_decorator(admin_view(require_permissions=('transmitters_management',)))
    def station_radio_transmitter_add(self, request, station_pk, transmitter_type):
        station = get_object_or_404(Station, pk=station_pk).get_subtype_instance()
        if isinstance(station, TVStation):
            messages.error(request, ugettext(u'TV stations not supported yet.'))
            return redirect(self.reverse('details', args=[station.pk]))
        TransmitterModel = {'FM': FMTransmitter, 'DAB': DABTransmitter}[transmitter_type]
        transmitter = TransmitterModel(station=station)
        use_existing_location_form = forms.UseExistingTransmitterLocationChoiceForm(data=request.POST or None)
        if use_existing_location_form.is_valid():
            use_existing_location = use_existing_location_form.cleaned_data['use_existing_location']
            create_new_location = not use_existing_location
        else:
            use_existing_location = create_new_location = False
        location_select_form = forms.TransmitterLocationChoiceForm(data=request.POST or None)
        location_form = forms.TransmitterLocationForm(data=request.POST or None, instance=TransmitterLocation(),
                                                      prefix='new_location')
        Form = modelform_factory(TransmitterModel, exclude=['station', 'location'])
        form = Form(data=request.POST or None, instance=transmitter)
        all_forms_valid = False
        redirect_to = self.reverse('details', args=[station.pk])
        if create_new_location:
            all_forms_valid = all([f.is_valid() for f in [location_form, form]])
        elif use_existing_location:
            all_forms_valid = all([f.is_valid() for f in [location_select_form, form]])
        if all_forms_valid:
            if create_new_location:
                location = location_form.save()
            else:
                location = location_select_form.cleaned_data['location']
            transmitter = form.save(commit=False)
            transmitter.location = location
            transmitter.save()
            messages.success(request, ugettext(u'Radio transmitter created successfully.'))

            return redirect(redirect_to)
        cancel_url = redirect_to
        context = self.get_context_data(request, form=form, location_select_form=location_select_form,
                                        use_existing_location_form=use_existing_location_form,
                                        location_form=location_form, cancel_url=cancel_url, station=station)
        templates = self._get_templates('radio_transmitter_add')
        return render_to_response(templates, request=request, context=context)

    @method_decorator(admin_view(require_permissions=('transmitters_management',)))
    def station_transmitter_edit(self, request, station_pk, pk):
        station = get_object_or_404(Station, pk=station_pk).get_subtype_instance()
        transmitter = get_object_or_404(Transmitter, pk=pk).get_subtype_instance()
        TransmitterForm = modelform_factory(type(transmitter), exclude=['station', 'location'])
        form = TransmitterForm(data=request.POST or None, instance=transmitter)
        redirect_to = self.reverse('details', args=[station.pk])
        if form.is_valid():
            form.save()
            messages.success(request, ugettext(u'Transmitter changed successfully.'))
            return redirect(redirect_to)
        cancel_url = redirect_to
        context = self.get_context_data(request, transmitter=transmitter, form=form,
                                        cancel_url=cancel_url, station=station)
        templates = self._get_templates('transmitter_edit')
        return render_to_response(templates, request=request, context=context)

    @method_decorator(admin_view(require_permissions=('transmitters_management',)))
    def station_transmitter_delete(self, request, station_pk, pk):
        station = get_object_or_404(Station, pk=station_pk).get_subtype_instance()
        transmitter = get_object_or_404(Transmitter, pk=pk).get_subtype_instance()
        redirect_to = self.reverse('details', args=[transmitter.pk])
        form = ConfirmationForm(data=request.POST or None)
        if form.is_valid():
            transmitter.delete()
            messages.success(request, ugettext(u'Transmitter successfully deleted.'))
            return redirect(redirect_to)
        cancel_url = redirect_to
        context = self.get_context_data(request, form=form, cancel_url=cancel_url, station=station,
                                        transmitter=transmitter)
        templates = self._get_templates('transmitter_delete')
        return render_to_response(templates, request=request, context=context)

    def get_urls(self):
        return patterns('',
            url(r'^$', self.index, name='%s-index' % self.prefix),
            url(r'^station/select-type/$', self.station_type_select, name='%s-type-select' % self.prefix),
            url(r'^station/add/(?P<typ>(radio|tv))/$', self.add, name='%s-add' % self.prefix),
            url(r'^station/(?P<pk>\d+)/$', self.details, name='%s-details' % self.prefix),
            url(r'^station/(?P<pk>\d+)/edit/$', self.edit, name='%s-edit' % self.prefix),
            url(r'^station/(?P<pk>\d+)/delete/$', self.delete, name='%s-delete' % self.prefix),
            url(r'^station/(?P<station_pk>\d+)/image/add/$',
                self.station_image_add, name='%s-image-add' % self.prefix),
            url(r'^station/(?P<station_pk>\d+)/image/(?P<pk>\d+)/edit/$',
                self.station_image_edit, name='%s-image-edit' % self.prefix),
            url(r'^station/(?P<station_pk>\d+)/image/(?P<pk>\d+)/delete/$',
                self.station_image_delete, name='%s-image-delete' % self.prefix),
            url(r'^station/(?P<station_pk>\d+)/transmitter/(?P<pk>\d+)/edit/$',
                self.station_transmitter_edit, name='%s-transmitter-edit' % self.prefix),

            url(r'^station/(?P<station_pk>\d+)/radio-transmitter/select-type/$',
                self.station_transmitter_type_select, name='%s-transmitter-type-select' % self.prefix),
            url(r'^station/(?P<station_pk>\d+)/radio-transmitter/(?P<transmitter_type>(FM|DAB))/add/$',
                self.station_radio_transmitter_add, name='%s-radio-transmitter-add' % self.prefix),
            url(r'^station/(?P<station_pk>\d+)/transmitter/(?P<pk>\d+)/delete/$',
                self.station_transmitter_delete, name='%s-transmitter-delete' % self.prefix),
        )


class TransmitterAdminApp(utils.ModelAdminApp):

    def __init__(self, station_app):
        self.station_app = station_app
        super(TransmitterAdminApp, self).__init__(model=Transmitter)

    @method_decorator(admin_view(require_permissions=('transmitters_management',)))
    def index(self, request):
        transmitters = Transmitter.objects.all()
        filter_form = forms.TransmitterFilterForm(data=request.GET, transmitters=transmitters)
        if filter_form.is_valid():
            transmitters = filter_form.search()
        context = self.get_context_data(request, transmitters=transmitters,
                                        filter_form=filter_form)
        templates = self._get_templates('index')
        return render_to_response(templates, request=request, context=context)

    @method_decorator(admin_view(require_permissions=('transmitters_management',)))
    def transmitter_type_select(self, request):
        form = forms.TransmitterTypeChoiceForm(data=request.POST or None)
        if form.is_valid():
            typ = form.cleaned_data['typ']
            return redirect(self.reverse('radio-transmitter-add', args=[typ]))
        cancel_url = self.station_app.reverse('index')
        context = self.get_context_data(request, form=form, cancel_url=cancel_url)
        templates = self._get_templates('type_select')
        return render_to_response(templates, request=request, context=context)

    @method_decorator(admin_view(require_permissions=('transmitters_management',)))
    def radio_transmitter_add(self, request, transmitter_type):
        TransmitterModel = {'FM': FMTransmitter, 'DAB': DABTransmitter}[transmitter_type]
        transmitter = TransmitterModel()
        use_existing_location_form = forms.UseExistingTransmitterLocationChoiceForm(data=request.POST or None)
        if use_existing_location_form.is_valid():
            use_existing_location = use_existing_location_form.cleaned_data['use_existing_location']
            create_new_location = not use_existing_location
        else:
            use_existing_location = create_new_location = False
        location_select_form = forms.TransmitterLocationChoiceForm(data=request.POST or None)
        location_form = forms.TransmitterLocationForm(data=request.POST or None, instance=TransmitterLocation())
        Form = modelform_factory(TransmitterModel, exclude=['location'])
        form = Form(data=request.POST or None, instance=transmitter)
        all_forms_valid = False

        if create_new_location:
            all_forms_valid = all([f.is_valid() for f in [location_form, form]])
        elif use_existing_location:
            all_forms_valid = all([f.is_valid() for f in [location_select_form, form]])

        if all_forms_valid:
            if create_new_location:
                location = location_form.save()
            else:
                location = location_select_form.cleaned_data['location']
            transmitter = form.save(commit=False)
            transmitter.location = location
            transmitter.save()
            messages.success(request, ugettext(u'Radio transmitter created successfully.'))
            redirect_to = self.reverse('details', args=[transmitter.pk])
            return redirect(redirect_to)
        cancel_url = self.reverse('index')
        context = self.get_context_data(request, form=form, location_select_form=location_select_form,
                                        use_existing_location_form=use_existing_location_form,
                                        location_form=location_form, cancel_url=cancel_url)
        templates = self._get_templates('radio_transmitter_add')
        return render_to_response(templates, request=request, context=context)

    @method_decorator(admin_view(require_permissions=('transmitters_management',)))
    def details(self, request, pk):
        transmitter = get_object_or_404(Transmitter, pk=pk)
        transmitter = transmitter.get_subtype_instance()
        context = self.get_context_data(request, transmitter=transmitter)
        if isinstance(transmitter, FMTransmitter):
            template = 'admin/transmitters/fm_transmitter_details.html'
        elif isinstance(transmitter, DABTransmitter):
            template = 'admin/transmitters/dab_transmitter_details.html'
        elif isinstance(transmitter, TVTransmitter):
            template = 'admin/transmitters/tv_transmitter_details.html'
        else:
            template = self._get_templates('details')
        return render_to_response(template, request=request, context=context)

    @method_decorator(admin_view(require_permissions=('transmitters_management',)))
    def transmitter_location_details(self, request, location_pk):
        transmitter_location = get_object_or_404(TransmitterLocation, pk=location_pk)
        transmitters = [t.get_subtype_instance() for t in transmitter_location.transmitters.all()]
        context = self.get_context_data(request, transmitter_location=transmitter_location,
                                        transmitters=transmitters)
        templates = self._get_templates('location_details')
        return render_to_response(templates, request=request, context=context)

    @method_decorator(admin_view(require_permissions=('transmitters_management',)))
    def transmitter_location_edit(self, request, location_pk):
        transmitter_location = get_object_or_404(TransmitterLocation, pk=location_pk)
        form = forms.TransmitterLocationForm(data=request.POST or None, instance=transmitter_location)
        redirect_to = self.reverse('location-details', args=[transmitter_location.pk])
        if form.is_valid():
            form.save()
            messages.success(request, ugettext(u'Transmitter location successfully changed.'))
            return redirect(redirect_to)
        cancel_url = redirect_to
        context = self.get_context_data(request, transmitter_location=transmitter_location, form=form,
                                        cancel_url=cancel_url)
        return render_to_response('admin/transmitters/transmitter_location_edit.html',
                                  request=request, context=context)

    @method_decorator(admin_view(require_permissions=('transmitters_management',)))
    def transmitter_broadcasting_add(self, request, pk):
        transmitter = get_object_or_404(Transmitter, pk=pk)
        transmitter = transmitter.get_subtype_instance()
        redirect_to = self.reverse('details', args=[transmitter.pk])
        use_existing_city_form = forms.UseExistingCityChoiceForm(data=request.POST or None)
        if use_existing_city_form.is_valid():
            use_existing_city = use_existing_city_form.cleaned_data['use_existing_city']
            create_city = not use_existing_city
        else:
            use_existing_city = create_city = False
        city_select_form = forms.CityChoiceForm(data=request.POST or None, transmitter=transmitter)
        city_form = forms.CityForm(data=request.POST or None, instance=City())
        broadcasting_form = forms.BroadcastingFormBase(data=request.POST or None, transmitter=transmitter)

        all_forms_valid = False
        if use_existing_city:
            all_forms_valid = all([f.is_valid() for f in [city_select_form, broadcasting_form]])
        elif create_city:
            all_forms_valid = all([f.is_valid() for f in [city_form, broadcasting_form]])

        if all_forms_valid:
            if create_city:
                city = city_form.save()
            else:
                city = city_select_form.cleaned_data['city']
            CityTransmitter.objects.create(city=city, transmitter=transmitter,
                                           quality=broadcasting_form.cleaned_data['quality'])
            messages.success(request, ugettext(u'Broadcasting info created successfully.'))
            return redirect(redirect_to)
        cancel_url = redirect_to
        context = self.get_context_data(request, transmitter=transmitter, city_form=city_form,
                                        city_select_form=city_select_form, broadcasting_form=broadcasting_form,
                                        cancel_url=cancel_url, use_existing_city_form=use_existing_city_form)
        templates = self._get_templates('broadcasting_add')
        return render_to_response(templates, request=request, context=context)

    @method_decorator(admin_view(require_permissions=('transmitters_management',)))
    def transmitter_edit(self, request, pk):
        transmitter = get_object_or_404(Transmitter, pk=pk)
        transmitter = transmitter.get_subtype_instance()
        TransmitterForm = modelform_factory(type(transmitter), exclude=['station', 'location'])
        form = TransmitterForm(data=request.POST or None, instance=transmitter)
        redirect_to = self.reverse('details', args=[transmitter.pk])
        if form.is_valid():
            form.save()
            messages.success(request, ugettext(u'Transmitter changed successfully.'))
            return redirect(redirect_to)
        cancel_url = redirect_to
        context = self.get_context_data(request, transmitter=transmitter, form=form,
                                        cancel_url=cancel_url)
        templates = self._get_templates('edit')
        return render_to_response(templates, request=request, context=context)

    @method_decorator(admin_view(require_permissions=('transmitters_management',)))
    def delete(self, request, pk):
        transmitter = get_object_or_404(Transmitter, pk=pk)
        form = ConfirmationForm(data=request.POST or None)
        if form.is_valid():
            transmitter.delete()
            messages.success(request, ugettext(u'Transmitter deleted successfully.'))
            return redirect(self.reverse('index'))
        cancel_url = self.reverse('details', args=[transmitter.pk])
        context = self.get_context_data(request, transmitter=transmitter, form=form, cancel_url=cancel_url)
        templates = self._get_templates('delete')
        return render_to_response(templates, request=request, context=context)

    @method_decorator(admin_view(require_permissions=('transmitters_management',)))
    def transmitter_broadcasting_delete(self, request, pk, city_pk):
        transmitter = get_object_or_404(Transmitter, pk=pk)
        city = get_object_or_404(City, pk=city_pk)
        transmitter = transmitter.get_subtype_instance()
        redirect_to = self.reverse('details', args=[transmitter.pk])
        form = ConfirmationForm(data=request.POST or None)
        if form.is_valid():
            broadcast = Broadcast.from_city_and_transmitter(city=city, transmitter=transmitter)
            broadcast.broadcaster.delete()
            messages.success(request, ugettext(u'Broadcasting info successfully deleted.'))
            return redirect(redirect_to)
        cancel_url = redirect_to
        context = self.get_context_data(request, form=form, cancel_url=cancel_url,
                                        city=city, transmitter=transmitter)
        templates = self._get_templates('broadcasting_delete')
        return render_to_response(templates, request=request, context=context)

    @method_decorator(admin_view(require_permissions=('transmitters_management',)))
    def city_details(self, request, city_pk):
        city = get_object_or_404(City, pk=city_pk)
        transmitters = [t.get_subtype_instance() for t in
                        city.transmitters.all()]
        context = self.get_context_data(request, city=city, transmitters=transmitters)
        templates = self._get_templates('city_details')
        return render_to_response(templates, request=request, context=context)

    @method_decorator(admin_view(require_permissions=('transmitters_management',)))
    def city_index(self, request):
        cities = City.objects.all()
        form = forms.CityFilterForm(data=request.GET or None, cities=cities)
        if form.is_valid():
            cities = form.search()
        context = self.get_context_data(request, filter_form=form, cities=cities)
        return render_to_response('admin/transmitters/city_index.html',
                                  request=request, context=context)

    @method_decorator(admin_view(require_permissions=('transmitters_management',)))
    def city_transmitter_select(self, request, city_pk):
        city = get_object_or_404(City, pk=city_pk)
        transmitters = Transmitter.objects.exclude(cities__in=[city]).distinct()
        filter_form = forms.TransmitterFilterForm(data=request.GET, transmitters=transmitters)
        if filter_form.is_valid():
            transmitters = filter_form.search()
        context = self.get_context_data(request, city=city, transmitters=transmitters, filter_form=filter_form)
        return render_to_response(
            'admin/transmitters/city_transmitter_select.html',
                                  request=request, context=context)

    @method_decorator(admin_view(require_permissions=('transmitters_management',)))
    def city_transmitter_add(self, request, city_pk, transmitter_pk):
        city = get_object_or_404(City, pk=city_pk)
        transmitter = get_object_or_404(Transmitter, pk=transmitter_pk).get_subtype_instance()
        if CityTransmitter.objects.filter(city=city, transmitter=transmitter).exists():
            raise Http404
        broadcasting_form = forms.BroadcastingFormBase(data=request.POST or None,
                                                       transmitter=transmitter)
        redirect_to = self.reverse('city-details', args=[city.pk])
        if broadcasting_form.is_valid():
            CityTransmitter.objects.create(city=city, transmitter=transmitter,
                                           quality=broadcasting_form.cleaned_data['quality'])
            messages.success(request, ugettext(u'Transmitter successfully added to city.'))
            return redirect(redirect_to)
        cancel_url = redirect_to
        context = self.get_context_data(request, form=broadcasting_form, city=city,
                                        transmitter=transmitter, cancel_url=cancel_url)
        return render_to_response('admin/transmitters/city_transmitter_add.html',
                                  request=request, context=context)

    @method_decorator(admin_view(require_permissions=('transmitters_management',)))
    def city_transmitter_delete(self, request, city_pk, transmitter_pk):
        city = get_object_or_404(City, pk=city_pk)
        transmitter = get_object_or_404(Transmitter, pk=transmitter_pk).get_subtype_instance()
        city_transmitter = get_object_or_404(CityTransmitter, city=city, transmitter=transmitter)
        form = ConfirmationForm(data=request.POST or None)
        redirect_to = self.reverse('city-details', args=[city.pk])
        if form.is_valid():
            city_transmitter.delete()
            messages.success(request, ugettext(u'Transmitter successfully removed from city.'))
            return redirect(redirect_to)
        cancel_url = redirect_to
        context = self.get_context_data(request, form=form, city=city,
                                        transmitter=transmitter, cancel_url=cancel_url)
        return render_to_response('admin/transmitters/city_transmitter_delete.html',
                                  request=request, context=context)

    def get_urls(self):
        return patterns('',
            url(r'^$', self.index, name='%s-index' % self.prefix),
            url(r'^transmitter/(?P<pk>\d+)/$', self.details, name='%s-details' % self.prefix),
            url(r'^transmitter/(?P<pk>\d+)/broadcasting/add/$',
                self.transmitter_broadcasting_add, name='%s-broadcasting-add' % self.prefix),
            url(r'^transmitter/(?P<pk>\d+)/broadcasting/city/(?P<city_pk>\d+)/delete/$',
                self.transmitter_broadcasting_delete, name='%s-broadcasting-delete' % self.prefix),
            url(r'^location/(?P<location_pk>\d+)/$', self.transmitter_location_details,
                name='%s-location-details' % self.prefix),
            url(r'^location/(?P<location_pk>\d+)/edit/$', self.transmitter_location_edit,
                name='%s-location-edit' % self.prefix),
            url(r'^radio-transmitter/select-type/$',
                self.transmitter_type_select, name='%s-type-select' % self.prefix),
            url(r'^radio-transmitter/(?P<transmitter_type>(FM|DAB))/add/$',
                self.radio_transmitter_add, name='%s-radio-transmitter-add' % self.prefix),
            url(r'^transmitter/(?P<pk>\d+)/edit/$',
                self.transmitter_edit, name='%s-edit' % self.prefix),
            url(r'^transmitter/(?P<pk>\d+)/delete/$',
                self.delete, name='%s-delete' % self.prefix),
            url(r'^cities/$', self.city_index, name='%s-city-index' % self.prefix),
            url(r'^cities/city/(?P<city_pk>\d+)/$', self.city_details,
                name='%s-city-details' % self.prefix),
            url(r'^cities/city/(?P<city_pk>\d+)/add-transmitter/$', self.city_transmitter_select,
                name='%s-city-transmitter-select' % self.prefix),
            url(r'^cities/city/(?P<city_pk>\d+)/add-transmitter/(?P<transmitter_pk>\d+)/$',
                self.city_transmitter_add,
                name='%s-city-transmitter-add' % self.prefix),
            url(r'^cities/city/(?P<city_pk>\d+)/delete-transmitter/(?P<transmitter_pk>\d+)/$',
                self.city_transmitter_delete,
                name='%s-city-transmitter-delete' % self.prefix)
        )


class StreamAdminApp(utils.ModelAdminApp):

    def __init__(self, station_app):
        self.station_app = station_app
        super(StreamAdminApp, self).__init__(model=Stream)

    @method_decorator(admin_view(require_permissions=('stations_management',)))
    def add(self, request, station_pk):
        station = get_object_or_404(Station, pk=station_pk).get_subtype_instance()
        StreamModel = AudioStream if isinstance(station, RadioStation) else VideoStream
        stream = StreamModel(station=station)
        Form = modelform_factory(StreamModel, exclude=['station'], form=forms.StreamAddForm)
        form = Form(data=request.POST or None, instance=stream)
        redirect_to = self.station_app.reverse('details', args=[station.pk])
        if form.is_valid():
            form.save()
            messages.success(request, ugettext(u'Stream created successfully.'))
            return redirect(redirect_to)
        cancel_url = redirect_to
        context = self.get_context_data(request, form=form, station=station,
                                        cancel_url=cancel_url)
        templates = self._get_templates('add')
        return render_to_response(templates, request=request, context=context)

    @method_decorator(admin_view(require_permissions=('stations_management',)))
    def edit(self, request, station_pk, stream_pk):
        station = get_object_or_404(Station, pk=station_pk).get_subtype_instance()
        stream = get_object_or_404(Stream, pk=stream_pk).get_subtype_instance()
        StreamModel = type(stream)
        Form = modelform_factory(StreamModel, exclude=['station'])
        form = Form(data=request.POST or None, instance=stream)
        redirect_to = self.station_app.reverse('details', args=[station.pk])
        if form.is_valid():
            form.save()
            messages.success(request, ugettext(u'Stream edited successfully.'))
            return redirect(redirect_to)
        cancel_url = redirect_to
        context = self.get_context_data(request, form=form, station=station,
                                        cancel_url=cancel_url, stream=stream)
        templates = self._get_templates('edit')
        return render_to_response(templates, request=request, context=context)

    @method_decorator(admin_view(require_permissions=('stations_management',)))
    def delete(self, request, station_pk, stream_pk):
        station = get_object_or_404(Station, pk=station_pk).get_subtype_instance()
        stream = get_object_or_404(Stream, pk=stream_pk).get_subtype_instance()
        redirect_to = self.station_app.reverse('details', args=[station.pk])
        form = ConfirmationForm(data=request.POST or None)
        if form.is_valid():
            stream.delete()
            messages.success(request, ugettext(u'Stream deleted successfully.'))
            return redirect(redirect_to)
        cancel_url = redirect_to
        context = self.get_context_data(request, form=form, station=station,
                                        cancel_url=cancel_url, stream=stream)
        templates = self._get_templates('delete')
        return render_to_response(templates, request=request, context=context)

    def get_urls(self):
        return patterns('',
            url(r'^station/(?P<station_pk>\d+)/streams/stream/add/$',
                self.add, name='%s-add' % self.prefix),
            url(r'^station/(?P<station_pk>\d+)/streams/stream/(?P<stream_pk>\d+)/edit/$',
                self.edit, name='%s-edit' % self.prefix),
            url(r'^station/(?P<station_pk>\d+)/streams/stream/(?P<stream_pk>\d+)/delete/$',
                self.delete, name='%s-delete' % self.prefix),
        )


class UserAdminApp(utils.ModelAdminApp):

    def __init__(self):
        super(UserAdminApp, self).__init__(model=User)

    @method_decorator(admin_required)
    def index(self, request):
        users = User.objects.all()
        form = forms.UserFilterForm(users=users, data=request.GET or None)
        if form.is_valid():
            users = form.search()
        context = self.get_context_data(request, users=users, filter_form=form)
        return render_to_response('admin/users/index.html', request=request, context=context)

    @method_decorator(admin_required)
    def details(self, request, user_pk):
        user = get_object_or_404(User, pk=user_pk)
        context = self.get_context_data(request, user_=user)
        return render_to_response('admin/users/details.html', request=request, context=context)

    @method_decorator(admin_required)
    def edit(self, request, user_pk):
        user = get_object_or_404(User, pk=user_pk)
        Form = modelform_factory(User, fields=['email', 'is_staff', 'is_admin',
                                               'stations_management', 'transmitters_management'])
        form = Form(data=request.POST or None, instance=user)
        redirect_to = self.reverse('details', args=[user.pk])
        if form.is_valid():
            form.save()
            messages.success(request, ugettext(u'User edited successfully.'))
            return redirect(redirect_to)
        cancel_url = redirect_to
        context = self.get_context_data(request, form=form, user_=user,
                                        cancel_url=cancel_url)
        return render_to_response('admin/users/edit.html', request=request, context=context)

    @method_decorator(admin_required)
    def password_change(self, request, user_pk):
        user = get_object_or_404(User, pk=user_pk)
        form = PasswordForm(data=request.POST or None)
        redirect_to = self.reverse('details', args=[user.pk])
        if form.is_valid():
            password = form.cleaned_data['password1']
            user.set_password(password)
            user.save()
            messages.success(request, ugettext(u'Password changed successfully.'))
            return redirect(redirect_to)
        cancel_url = redirect_to
        context = self.get_context_data(request, form=form, user_=user,
                                        cancel_url=cancel_url)
        return render_to_response('admin/users/password_change.html', request=request, context=context)


    def get_urls(self):
        return patterns('',
            url(r'^$', self.index, name='%s-index' % self.prefix),
            url(r'^user/(?P<user_pk>\d+)/$', self.details, name='%s-details' % self.prefix),
            url(r'^user/(?P<user_pk>\d+)/edit/$', self.edit, name='%s-edit' % self.prefix),
            url(r'^user/(?P<user_pk>\d+)/change-password/$', self.password_change, name='%s-password-change' % self.prefix),
        )


class ActionAdminApp(utils.ModelAdminApp):

    def __init__(self):
        super(ActionAdminApp, self).__init__(model=Action)

    @method_decorator(admin_required)
    def index(self, request):
        actions = Action.objects.all()
        context = self.get_context_data(request, actions=actions)
        return render_to_response('admin/logs/index.html', request=request, context=context)

    def get_urls(self):
        return patterns('',
            url(r'^$', self.index, name='%s-index' % self.prefix),
        )



station_app = StationAdminApp()
transmitter_app = TransmitterAdminApp(station_app=station_app)
stream_app = StreamAdminApp(station_app=station_app)
user_app = UserAdminApp()
action_app = ActionAdminApp()


def test_exception(request):
    raise Exception('test')