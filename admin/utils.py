#-*- coding: utf-8 -*-
from __future__ import absolute_import
from django.utils.decorators import method_decorator

from users.decorators import admin_required
from templating import render_to_response
from utils import app


class ModelAdminApp(app.App):

    namespace = 'admin'

    def __init__(self, model, **kwargs):
        self.model = model
        self.prefix = model._meta.object_name.lower()
        kwargs['app_name'] = kwargs.pop('app_name', self.model._meta.app_label)
        super(ModelAdminApp, self).__init__(**kwargs)

    def get_context_data(self, request, **kwargs):
        return super(ModelAdminApp, self).get_context_data(
            request=request, **kwargs
        )

    def _get_templates(self, suffix):
        return [
            'admin/%s/%s_%s.html' % (self.app_name, self.prefix, suffix),
            'admin/%s/%s.html' % (self.app_name, suffix),
            'admin/%s.html' % suffix
        ]

    def reverse(self, view_name, *args, **kwargs):
        return super(ModelAdminApp, self).reverse('%s-%s' % (self.prefix, view_name), *args, **kwargs)
