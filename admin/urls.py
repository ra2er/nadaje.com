from __future__ import absolute_import
import os
import yaml
from django.conf import settings
from django.conf.urls import url, patterns, include

from . import forms
from . import views
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.shortcuts import redirect
from django.utils.translation import ugettext
from templating import render_to_response
from users.decorators import admin_required


@admin_required
def google_ads_edit(request):
    ads_file = open(os.path.join(settings.PROJECT_ROOT, 'static', 'google_ads.yaml'), 'r')
    google_ads_config = yaml.load(ads_file.read())
    ads_file.close()
    client_id_form = forms.GoogleAdsForm(data=request.POST or None, initial={'client_id': google_ads_config['client_id']})
    ad_forms = []
    redirect_to = reverse('admin:google-ads')
    for ad in google_ads_config['ads'].items():
        ad_forms.append(forms.GoogleAdEditForm(data=request.POST or None, ad=ad[1], prefix=ad[0]))
    if all([client_id_form.is_valid()] + [f.is_valid() for f in ad_forms]):
        google_ads_config['client_id'] = client_id_form.cleaned_data['client_id']
        for f in ad_forms:
            google_ads_config['ads'][f.prefix].update(**f.cleaned_data)
        out = yaml.dump(google_ads_config)
        ads_file = open(os.path.join(settings.PROJECT_ROOT, 'static', 'google_ads.yaml'), 'w')
        ads_file.write(out)
        ads_file.close()
        messages.success(request, ugettext('Google ads updated successfully.'))
        return redirect(redirect_to)
    cancel_url = redirect_to
    context = {
        'client_id_form': client_id_form,
        'ad_forms': ad_forms,
        'cancel_url': cancel_url
    }
    return render_to_response('admin/google_ads_edit.html', request=request, context=context)

@admin_required
def google_ads(request):
    ads_file = open(os.path.join(settings.PROJECT_ROOT, 'static', 'google_ads.yaml'), 'r')
    google_ads_config = yaml.load(ads_file.read())
    context = {
        'google_ads_config': google_ads_config
    }
    ads_file.close()
    return render_to_response('admin/google_ads.html', request=request, context=context)


subpatterns = patterns('',
    url(r'^stations/', include(views.station_app.get_urls())),
    url(r'^streams/', include(views.stream_app.get_urls())),
    url(r'^transmitters/', include(views.transmitter_app.get_urls())),
    url(r'^google-ads/$', google_ads, name='google-ads'),
    url(r'^google-ads/edit/$', google_ads_edit, name='google-ads-edit'),
    url(r'^users/', include(views.user_app.get_urls())),
    url(r'^actions/', include(views.action_app.get_urls())),
)

urlpatterns = patterns('',
    url(r'^$', views.station_app.index, name='admin'),
    url(r'^', include(subpatterns, namespace='admin')),
    url(r'^test-exception/$', views.test_exception),
)