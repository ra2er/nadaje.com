#-*- coding: utf-8 -*-
from urllib import quote
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.core.urlresolvers import reverse

from .views import station_app
from stations.models import Station, RadioStation
from testing import JinjaViewsTestCase, Client
from users.models import User


class ViewsTestCase(JinjaViewsTestCase):

    DEFAULT_PASSWORD = 'password'

    def setUp(self):
        super(ViewsTestCase, self).setUp()

        self.station = RadioStation.objects.create(name='test')
        self.admin_user = User.objects.create_user(email='admin@example.com',
                                                   password=self.DEFAULT_PASSWORD,
                                                   is_admin=True)
        self.admin_client = Client()
        self.admin_client.login(email=self.admin_user.email,
                                password=self.DEFAULT_PASSWORD)

    def test_admin_redirects_to_login_view_for_anonymous_user(self):
        dashboard_url = reverse('admin')
        response = self._test_GET_status(dashboard_url,
            status_code=302, client_instance=self.client)
        self.assertRedirects(response,
                             '%s?%s=%s' % (reverse('user-login'),
                                           quote(REDIRECT_FIELD_NAME, '/'),
                                           quote(dashboard_url, '/')))

    def test_admin(self):
        admin_url = reverse('admin')
        self._test_GET_status(admin_url, client_instance=self.admin_client)

    def test_station_details(self):
        station_details_url = station_app.reverse('details', args=[self.station.pk])
        self._test_GET_status(station_details_url, client_instance=self.admin_client)

    def test_radio_station_add(self):
        station_add_url = station_app.reverse('add', args=['radio'])
        self._test_GET_status(station_add_url, client_instance=self.admin_client)
        response = self._test_POST_status(station_add_url, client_instance=self.admin_client, data={
            'name': 'new',
            'typ': 'FM'
        })
        self.assertTrue(Station.objects.filter(name='new').exists())
        station = Station.objects.get(name='new')
        self.assertRedirects(response, station_app.reverse('details', args=[station.pk]))

    def test_radio_station_edit(self):
        station = self.station
        new_name = self.station.name + 'new'
        station_edit_url = station_app.reverse('edit', args=[self.station.pk])
        self._test_GET_status(station_edit_url, client_instance=self.admin_client)
        response = self._test_POST_status(station_edit_url, client_instance=self.admin_client, data={
            'name': new_name,
            'typ': 'FM'
        })
        self.assertTrue(Station.objects.filter(name=new_name).exists())
        self.assertRedirects(response, station_app.reverse('details', args=[station.pk]))


    def test_radio_station_delete(self):
        station = self.station
        station_delete_url = station_app.reverse('delete', args=[self.station.pk])
        self._test_GET_status(station_delete_url, client_instance=self.admin_client)
        response = self._test_POST_status(station_delete_url, client_instance=self.admin_client, data={
            'confirm': True
        })
        self.assertFalse(Station.objects.filter(pk=station.pk).exists())
        self.assertRedirects(response, station_app.reverse('index'))