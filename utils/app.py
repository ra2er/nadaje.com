from django.conf.urls import url, patterns
from django.core.urlresolvers import reverse
from django.shortcuts import redirect


class App(object):

    app_name = None
    namespace = None

    def __init__(self, app_name=None, namespace=None):
        self.app_name = app_name or self.app_name
        self.namespace = namespace or self.namespace

    def get_context_data(self, request, **kwargs):
        context = {
            'current_app': self.app_name
        }
        context.update(kwargs)
        return context

    def redirect(self, to, *args, **kwargs):
        uri = self.reverse(to, args=args, kwargs=kwargs)
        return redirect(uri)

    def reverse(self, to, args=None, kwargs=None):
        to = '%s:%s' % (self.namespace, to)
        return reverse(to, args=args, kwargs=kwargs, current_app=self.app_name)

    def get_urls(self):
        raise NotImplementedError()

    @property
    def urls(self):
        return self.get_urls()
