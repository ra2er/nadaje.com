#-*- coding: utf-8 -*-
import urlparse
from django.conf import settings
from django.utils.safestring import mark_safe
from . import premailer
import templating
from mailmachine import enqueue, MailQueue

def get_mail_queue():
    mail_queue = MailQueue(settings.MAIL_QUEUE)
    return mail_queue

_env_for_plain_text = None
def _get_env_for_plain_text():
    global _env_for_plain_text
    if _env_for_plain_text is None:
        from urls import register_filters
        # allow full control of newlines
        _env_for_plain_text = templating.create_env(trim_blocks=True)
        register_filters(_env_for_plain_text)
    return _env_for_plain_text

def prepare_message(label, emails, extra_context=None,
                    send_as=None, reply_to=None, attachments=None):

    parts = {
        'subject': 'mails/%s/subject.txt' % label,
        'body_txt': 'mails/%s/body.txt' % label,
        'body_html': 'mails/%s/body.html' % label,
    }
    context = {
        'STATIC_URL': settings.STATIC_URL,
    }
    if extra_context:
        context.update(extra_context)

    subject = templating.render_to_string(parts['subject'], context)
    context['subject'] = subject

    env_for_plain_text = _get_env_for_plain_text()
    template = templating.get_template(parts['body_txt'],
                                       env=env_for_plain_text)
    body_txt = templating.render_to_string(template, context)

    body_html = templating.render_to_string(parts['body_html'], context)
    subject = ''.join(subject.splitlines())
    base_url = urlparse.urlunparse(('http', settings.DOMAIN, '',
                                    '', '', ''))
    pm = premailer.Premailer(body_html, base_url=base_url)
    body_html = mark_safe(pm.transform())
    sender = reply_to or settings.DEFAULT_FROM_EMAIL
    message = {
        'subject': subject,
        'body': body_txt,
        'from_email': sender,
        'recipients': emails,
        'alternatives': [(body_html, 'text/html')],
        'attachments': attachments,
    }
    return message

def send_mail(msg):
    mail_queue = get_mail_queue()
    enqueue(mail_queue=mail_queue, **msg)

def prepare_and_send(label, emails, extra_context=None,
                     send_as=None, reply_to=None, attachments=None):
    message = prepare_message(label, emails, extra_context, send_as, reply_to, attachments)
    send_mail(message)


__all__ = ['prepare_and_send']
