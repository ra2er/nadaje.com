from django.core import validators
from django.utils.translation import ugettext_lazy as _
import re


lowercase_re = re.compile('^[^A-Z]+$')
lowercase_validator = validators.RegexValidator(lowercase_re,
                                                _(u'Use only lowercase '
                                                'letters in your email address.'),
                                                'uppercase-found')
