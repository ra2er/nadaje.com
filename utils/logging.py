from __future__ import absolute_import
from django.conf import settings
import logging.handlers
from mailmachine.logging import EnqueueMailLoggingHandler
import socket


class SysLogHandler(logging.handlers.SysLogHandler):

    def __init__(self, *args, **kwargs):
        self.split_lines = kwargs.pop('split_lines', False)
        super(SysLogHandler, self).__init__(*args, **kwargs)

    def _emit_msg(self, msg, record):
        msg = msg + '\000'
        """
        We need to convert record level to lowercase, maybe this will
        change in the future.
        """
        prio = '<%d>' % self.encodePriority(self.facility,
                                            self.mapPriority(record.levelname))
        # Message is a string. Convert to bytes as required by RFC 5424
        if type(msg) is unicode:
            msg = msg.encode('utf-8')
        msg = prio + msg
        try:
            if self.unixsocket:
                try:
                    self.socket.send(msg)
                except socket.error:
                    self._connect_unixsocket(self.address)
                    self.socket.send(msg)
            elif self.socktype == socket.SOCK_DGRAM:
                self.socket.sendto(msg, self.address)
            else:
                self.socket.sendall(msg)
        except (KeyboardInterrupt, SystemExit):
            raise
        except:
            self.handleError(record)

    def emit(self, record):
        msg = self.format(record)
        if self.split_lines:
            for m in msg.split('\n'):
                self._emit_msg(m, record)
        else:
            self._emit_msg(msg, record)


class AdminEmailHandler(EnqueueMailLoggingHandler):

    def __init__(self, *args, **kwargs):
        def subject(record):
            try:
                return ('%s (%s IP): %s' %
                        (record.levelname,
                         ((record.request.META.get('REMOTE_ADDR') in settings.INTERNAL_IPS and
                           'internal') or
                          'EXTERNAL'), record.getMessage()))
            except Exception:
                return '%s: %s' % (
                    record.levelname,
                    record.getMessage()
                )
        kwargs['subject'] = subject
        super(AdminEmailHandler, self).__init__(*args, **kwargs)
