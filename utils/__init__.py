#-*- coding: utf-8 -*-
import re
import datetime
import pytz


def slugify(string):
    remove_list = ['a', 'an', 'as', 'at', 'before', 'but',
                   'by', 'for', 'from', 'is', 'in', 'into',
                   'like', 'of', 'off', 'on', 'onto', 'per',
                   'since', 'than', 'the', 'this', 'that',
                   'to', 'up', 'via', 'with']
    slug = string
    for word in remove_list:
        slug = re.sub(r'\b' + word + r'\b', '', string)
    slug = re.sub('[^\w\s-]', '', slug).strip().lower()
    slug = re.sub('\s+', '-', slug)
    return slug


def utcnow():
    return datetime.datetime.utcnow().replace(tzinfo=pytz.UTC)