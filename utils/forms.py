import re
from django import forms
from django.utils.translation import ugettext_lazy as _, ugettext


class ConfirmationForm(forms.Form):

    confirm = forms.BooleanField(
        label=_('Confirm'),
        error_messages={'required': _('You need to confirm '
                                      'before proceeding.')}
    )


class SelfAwareFormMixin(forms.Form):
    """
       This form validates only when it was submitted (request is marked)...
       don't forget to set unique prefix for every form on page.
    """

    request_marker = forms.CharField(widget=forms.HiddenInput(), initial='marker',
                                     required=False)

    def __init__(self, *args, **kwargs):
        super(SelfAwareFormMixin, self).__init__(*args, **kwargs)
        self.is_bound = (self.is_bound and
                         self.add_prefix('request_marker') in self.data)
        if not self.is_bound:
            self.data = None


class QueryField(forms.Field):

    def __init__(self, *args, **kwargs):
        self.min_word_length = kwargs.pop('min_word_length', 3)
        kwargs['help_text'] = kwargs.pop('help_text',
            ugettext('Words shorter than %(min)i letters are ignored during search.') % {'min': self.min_word_length})
        super(QueryField, self).__init__(*args, **kwargs)

    def to_python(self, value):
        if not value:
            return []
        return re.findall('[^\s]{%i,}' % self.min_word_length, value)


class ContactForm(forms.Form):

    email = forms.EmailField(label=_('E-mail'))
    subject = forms.CharField(label=_('Subject'), max_length=200)
    message = forms.CharField(label=_('Message'), max_length=1000, widget=forms.Textarea())

