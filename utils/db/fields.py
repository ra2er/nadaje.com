#-*- coding: utf-8 -*-
from django.conf import settings
from django.db.models.fields import CharField


class LanguageField(CharField):

    def __init__(self, *args, **kwargs):
        super(LanguageField, self).__init__(*args, **kwargs)
        self._choices = settings.LANGUAGES[1:]
        self.max_length = 2