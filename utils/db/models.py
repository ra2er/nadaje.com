from django.db import models
from django.dispatch import receiver

class Subtyped(models.Model):

    subtype_attr = models.CharField(max_length=500, editable=False)

    class Meta:
        abstract = True

    def get_subtype_instance(self):
        subtype = self
        path = self.subtype_attr.split()
        whoami = self._meta.module_name
        remaining = path[path.index(whoami)+1:]
        for r in remaining:
            subtype = getattr(subtype, r)
        return subtype

    def store_subtype(self):
        if not self.id:
            path = [self]
            parents = self._meta.parents.keys()
            while parents:
                parent = parents[0]
                path.append(parent)
                parents = parent._meta.parents.keys()
            path = [p._meta.module_name for p in reversed(path)]
            self.subtype_attr = ' '.join(path)


@receiver(models.signals.pre_save)
def _store_content_type(sender, instance, **kwargs):
    if isinstance(instance, Subtyped):
        instance.store_subtype()

