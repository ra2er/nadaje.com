from templating import register_filter


def image_path(image):
    path = image.image.path.split('media')[-1]
    return path


def register_filters(env):
    register_filter(image_path, name='image.path', env=env)