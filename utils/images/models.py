#-*- coding:utf-8 -*-
import hashlib
import os
from django.db import models
from south.modelsinspector import add_introspection_rules
from django.dispatch.dispatcher import receiver

try:
    from cStringIO import StringIO
except:
    from StringIO import StringIO
from django.core.files.uploadedfile import InMemoryUploadedFile
from . import scale_and_crop
from . import IMAGE_SIZES
from django.utils.translation import ugettext_lazy as _


def hashed_upload_to(prefix, instance, filename):
    hasher = hashlib.md5()
    for chunk in instance.image.chunks():
        hasher.update(chunk)
    hsh = hasher.hexdigest()
    base, ext = os.path.splitext(filename)
    return '%(prefix)s%(first)s/%(second)s/%(hash)s/%(base)s%(ext)s' % {
        'prefix': prefix,
        'first': hsh[0],
        'second': hsh[1],
        'hash': hsh,
        'base': base,
        'ext': ext
    }


def image_upload_to(instance, filename, **kwargs):
    return hashed_upload_to('images/original/md5/', instance, filename)


class Image(models.Model):

    image = models.ImageField(_('image'), upload_to=image_upload_to,
                              height_field='height', width_field='width',
                              max_length=500)
    height = models.PositiveIntegerField(default=0, editable=False)
    width = models.PositiveIntegerField(default=0, editable=False)
    
    def get_by_size(self, size):
        return self.thumbnails.get(size=size)
    
    def get_absolute_url(self, size=None):
        if not size:
            return self.image.url
        try:
            return self.get_by_size(size).image.url
        except Thumbnail.DoesNotExist:
            try:
                thumbnail = self.thumbnails.get_or_create_at_size(self.pk, size)
            except (IOError, OSError):
                return ''
            return thumbnail.image.url
        except Thumbnail.MultipleObjectsReturned:
            self.thumbnails.filter(size=size).delete()
            return self.thumbnails.get_or_create_at_size(self.pk, size)

    
def thumbnail_upload_to(instance=None, filename=None, **kwargs):
    return hashed_upload_to('images/thumbnail/md5/', instance, filename)


class ThumbnailManager(models.Manager):

    def get_or_create_at_size(self, image_id, size):
        image = Image.objects.get(id=image_id)
        if not size in IMAGE_SIZES:
            raise ValueError('Received unknown size: %s' % size)
        try:
            thumbnail = image.get_by_size(size)
        except Thumbnail.DoesNotExist:
            img = scale_and_crop(image.image, **IMAGE_SIZES[size])
            # save to memory
            buf = StringIO()
            img.save(buf, img.format, **img.info)
            # and save to storage
            original_dir, original_file = os.path.split(image.image.name)
            thumb_file = InMemoryUploadedFile(buf, 'image', original_file, None,
                                              buf.tell(), None)
            thumbnail = image.thumbnails.create(size=size, image=thumb_file)
        return thumbnail


class Thumbnail(models.Model):

    original = models.ForeignKey(Image, related_name='thumbnails')
    image = models.ImageField(upload_to=thumbnail_upload_to,
                              height_field='height', width_field='width',
                              max_length=500)
    size = models.CharField(max_length=100)
    height = models.PositiveIntegerField(default=0, editable=False)
    width = models.PositiveIntegerField(default=0, editable=False)
    
    objects = ThumbnailManager()
    
    class Meta:
        unique_together = ('image', 'size')
        
    def get_absolute_url(self):
        return self.image.url

    
def original_changed(sender, instance, created, **kwargs):
    if isinstance(instance, Image):
        instance.thumbnails.all().delete()


models.signals.post_save.connect(original_changed, sender=Image)


def auto_delete_file_on_delete(sender, instance, **kwargs):
    if instance.image:
        if os.path.isfile(instance.image.path):
            os.remove(instance.image.path)


def auto_delete_file_on_change(sender, instance, **kwargs):
    if instance.pk:
        try:
            old_file = Image.objects.get(pk=instance.pk).image
            new_file = instance.image
            if not old_file == new_file and os.path.isfile(old_file.path):
                os.remove(old_file.path)
        except Image.DoesNotExist:
            pass


for Model in [Image, Thumbnail]:
    models.signals.post_delete.connect(auto_delete_file_on_delete, sender=Model)
    models.signals.pre_save.connect(auto_delete_file_on_change, sender=Model)