#!/bin/sh

pybabel extract -F locale/babel.cfg -o locale/translation.pot . && pybabel update -l pl -i locale/translation.pot -D django -d locale && poedit locale/pl/LC_MESSAGES/django.po && pybabel compile -l pl -d locale -D django
