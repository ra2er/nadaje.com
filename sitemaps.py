from django.contrib import sitemaps
from django.core.urlresolvers import reverse


class FlatPageSitemap(sitemaps.Sitemap):

    priority = 0.5
    changefreq = 'daily'

    def items(self):
        return ['home-page',]

    def location(self, item):
        return reverse(item)