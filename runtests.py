#!/usr/bin/env python
from __future__ import absolute_import
import argparse
from django.conf import settings
from django.core.management import call_command
import os


os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'


class DefaultListAction(argparse.Action):

    default_choices = ()

    def __call__(self, parser, namespace, values, option_string=None):
        if values:
            choices = self.choices or self.default_choices
            for value in values:
                if all(not value.startswith(c) for c in choices):
                    message = ("invalid choice: {0!r} (choose from {1})"
                               .format(value,
                                       ', '.join([repr(action)
                                                  for action in choices])))

                    raise argparse.ArgumentError(self, message)
            setattr(namespace, self.dest, values)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog='runtests')
    apps = [
        app for app in settings.APPS
    ]
    A = type('Action', (DefaultListAction,), {'choices': list(apps)})
    parser.add_argument('apps', action=type('Action', (DefaultListAction,), {'default_choices': list(apps)}),
                        nargs='*')
    parser.add_argument('-q', '--sqllite', action='store_true', help='Force test db to be sqlite in memory instance',
                        default=False, dest='quick')
    parser.add_argument('-m', '--no-migrations', action='store_true', help='Force test db without migrations',
                        default=False, dest='no_migrations')
    args = parser.parse_args()
    apps = args.apps or apps

    if args.quick:
        settings.DATABASES['default'] = {'ENGINE': 'django.db.backends.sqlite3'}
    if args.no_migrations:
        settings.SOUTH_TESTS_MIGRATE = False

    call_command('test', *apps)

