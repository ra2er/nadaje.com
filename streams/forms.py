#-*- coding: utf-8
from __future__ import absolute_import
from django import forms
from django.core.exceptions import ValidationError
from django.forms import CharField
from django.forms.models import BaseInlineFormSet
from django.utils.translation import ugettext_lazy as _
import urlparse

from .validators import StreamURLValidator
from .utils import get_stream_headers
from .import models


class StreamURLField(CharField):

    default_error_messages = {
        'invalid': _(u'Enter a valid stream URL (probably http, rtsp or mms).'),
    }

    def __init__(self, max_length=None, min_length=None, *args, **kwargs):
        super(StreamURLField, self).__init__(max_length, min_length, *args,
                                       **kwargs)
        self.validators.append(StreamURLValidator())

    def to_python(self, value):

        def split_url(url):
            """
            Returns a list of url parts via ``urlparse.urlsplit`` (or raises a
            ``ValidationError`` exception for certain).
            """
            try:
                return list(urlparse.urlsplit(url))
            except ValueError:
                # urlparse.urlsplit can raise a ValueError with some
                # misformatted URLs.
                raise ValidationError(self.error_messages['invalid'])

        value = super(StreamURLField, self).to_python(value)
        if value:
            url_fields = split_url(value)
            if not url_fields[0]:
                # If no URL scheme given, assume rtsp://
                url_fields[0] = 'rtsp'
            if not url_fields[1]:
                # Assume that if no domain is provided, that the path segment
                # contains the domain.
                url_fields[1] = url_fields[2]
                url_fields[2] = ''
                # Rebuild the url_fields list, since the domain segment may now
                # contain the path too.
                url_fields = split_url(urlparse.urlunsplit(url_fields))
            if not url_fields[2]:
                # the path portion may need to be added before query params
                url_fields[2] = '/'
            value = urlparse.urlunsplit(url_fields)
        return value

class AudioStreamForm(forms.ModelForm):

    class Meta:
        model = models.AudioStream
        fields = ['url']
        widgets = {
            'url': forms.TextInput(attrs={'placeholder': 'url'})
        }

    def check_stream(self, url):
        stream_headers = get_stream_headers(url)
        if len(stream_headers['errorlist']) > 0:
            self.errors['url'] = self.error_class(stream_headers['errorlist'])
        return stream_headers

    def clean(self):
        cleaned_data = super(AudioStreamForm, self).clean()
        url = cleaned_data.get('url')
        if url:
            stream = self.check_stream(url)
            self.instance.url = url
            self.instance.streamer = stream['streamer']
            self.instance.mimetype = stream['mimetype']
            self.instance.source = url
            self.instance.codec = stream['codec']
            self.instance.bitrate = stream['bitrate']
        return cleaned_data


class StreamInlineFormset(BaseInlineFormSet):

    def clean(self):
        super(StreamInlineFormset, self).clean()
        if not self.is_valid():
            return
        self.deleted_forms
        if any((e and i not in self._deleted_form_indexes) for i,e in enumerate(self.errors)):
            return
        if all(((hasattr(f, 'cleaned_data') and not f.cleaned_data) or i in self._deleted_form_indexes) for i,f in enumerate(self.forms)):
            raise forms.ValidationError(_('At least one %s is required.') % self.model._meta.verbose_name)