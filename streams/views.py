import simplejson
import urlparse
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from streams.models import AudioStream
from streams.forms import AudioStreamForm


def audio_stream_metadata(request, stream_pk):
    stream = get_object_or_404(AudioStream, pk=stream_pk)
    parts = urlparse.urlparse(stream.url)
    hostname, port = parts.split(':')
    #todo: get playing track


def stream_check(request):
    form = AudioStreamForm(data=request.GET or None)
    stream = {}
    if form.is_valid():
        stream = form.check_stream(form.cleaned_data['url'])
    return HttpResponse(simplejson.dumps(stream))


