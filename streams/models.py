#-*- coding: utf-8 -*-
from __future__ import absolute_import
from django.db import models
from django.utils.translation import ugettext_lazy as _
from stations.models import Station
from .fields import StreamURLField
from utils.db.models import Subtyped


class Stream(Subtyped):

    station = models.ForeignKey(Station, related_name='streams')
    url = StreamURLField()
    source = models.URLField(_('source'), blank=True)
    bitrate = models.CharField(_('bitrate'), max_length=10, blank=True)
    mimetype = models.CharField(_('mime type'), max_length=100, blank=True)
    active = models.BooleanField(_('active'), default=True)
    browser_playable = models.BooleanField()

    def __unicode__(self):
        return self.station.name

    class Meta:
        unique_together = (('station', 'url'),)


class AudioStream(Stream):

    codec = models.CharField(_('codec'), blank=True, max_length=10,
                             choices=[('aacp', 'AAC+'), ('mp3', 'MP3'),
                                      ('ogg', 'OGG'), ('wma', 'WMA')])
    SERVER_CHOICES = (
        ('shoutcast1', 'Shoutcast 1'),
        ('shoutcast2', 'Shoutcast 2'),
        ('icecast', 'IceCast'),
    )
    streamer = models.CharField(_('streamer'), max_length=max([len(c[0]) for c in SERVER_CHOICES]), blank=True,
                                choices=SERVER_CHOICES)

    def __unicode__(self):
        return u'%s %s kbps' % (self.get_codec_display(), self.bitrate)

    def get_stream_url(self):
        if self.streamer == 'shoutcast1' and self.codec in ['mp3', 'ogg']:
            return self.url.strip('/') + '/;stream'
        return self.url


class VideoStream(Stream):

    pass