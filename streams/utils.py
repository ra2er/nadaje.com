import socket
import urlparse
import BeautifulSoup
from django.utils.translation import ugettext
import urllib2
import re
import requests


def get_stream_headers(url):
    meta_dict = {'errorlist':[], 'url':url, 'mimetype':'', 'bitrate':'', 'codec':'', 'streamer':''}
    try:
        sock = urllib2.urlopen(url, timeout=20)
        headers = sock.info().headers
        if len(headers) < 1:
            shoutcast_stream = sock.read(512)
            shoutcast_stream = re.sub(r'\r', '', shoutcast_stream)
            meta_list = shoutcast_stream.split('\n')
            if re.sub('\r', '', meta_list[0]) == 'ICY 400 Server Full':
                meta_dict['errorlist'].append('Server is full')
            elif re.sub('\r', '', meta_list[0]) == 'ICY 200 OK':
                pass
            meta_dict['streamer'] = 'shoutcast1' # let admin decide if it is sc1 or sc2
            for i in meta_list:
                if len(i.split(':')) > 1:
                    if i.split(':')[0].lower() == 'content-type':
                        meta_dict['mimetype'] = re.sub('\s', '', i.split(':')[1])
                    if i.split(':')[0].lower() == 'icy-br':
                        meta_dict['bitrate'] = i.split(':')[1]
        else:
            for i in headers:
                if len(i.split(':')) > 1:
                    if i.split(':')[0].lower() == 'content-type':
                        meta_dict['mimetype'] = re.sub(r'\s', '', i.split(':')[1])
                    if i.split(':')[0].lower() == 'icy-br':
                        meta_dict['bitrate'] = re.sub(r'\s', '', i.split(':')[1])
            streamer = sock.info().getheader('server')
            if streamer and 'icecast' in streamer.lower():
                meta_dict['streamer'] = 'icecast'
    except urllib2.URLError:
        meta_dict['errorlist'].append(ugettext('Connection timed out or dead link.'))

    if meta_dict['mimetype'] == 'audio/mpeg':
        meta_dict['codec'] = 'mp3'
    elif meta_dict['mimetype'] == 'application/ogg' or meta_dict['mimetype'] == 'audio/ogg':
        meta_dict['codec'] = 'ogg'
    elif meta_dict['mimetype'] == 'audio/aacp':
        meta_dict['codec'] = 'aac+'
    elif meta_dict['mimetype'] == 'audio/aac':
        meta_dict['codec'] = 'aac'
    elif meta_dict['mimetype'] == 'audio/x-ms-wma' or meta_dict['mimetype'] == 'video/x-ms-asf':
        meta_dict['codec'] = 'wma'
    elif meta_dict['mimetype'] == 'audio/x-pn-realaudio' or meta_dict['mimetype'] == 'audio/vnd.rn-realaudio' or meta_dict['mimetype'] == 'audio/x-pm-realaudio-plugin' or meta_dict['mimetype'] == 'audio/x-realaudio':
        meta_dict['codec'] = 'realaudio'
    else:
        meta_dict['codec'] = None
        meta_dict['errorlist'].append(ugettext('Unsupported or invalid stream.'))
    if not meta_dict.has_key('bitrate'):
        meta_dict['bitrate'] = None
    return meta_dict