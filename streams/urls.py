#-*- coding: utf-8 -*-
from django.conf.urls import url, patterns

from . import views

urlpatterns = patterns('',
    url(r'^stream/audio/(?P<stream_pk>\d+)/metadata/$', views.audio_stream_metadata),
    url(r'^stream/check-url/$', views.stream_check)
)

