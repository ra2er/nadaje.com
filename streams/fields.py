from __future__ import absolute_import
from django.db.models import CharField
from django.utils.translation import ugettext_lazy as _
from south.modelsinspector import add_introspection_rules
from streams.validators import StreamURLValidator


class StreamURLField(CharField):

    description = _("Audio/Video stream URL")

    def __init__(self, verbose_name=None, name=None, **kwargs):
        kwargs['max_length'] = kwargs.get('max_length', 200)
        CharField.__init__(self, verbose_name, name, **kwargs)
        self.validators.append(StreamURLValidator())

    def formfield(self, **kwargs):
        from streams.forms import StreamURLField as StreamURLFormField
        defaults = {
            'form_class': StreamURLFormField,
        }
        defaults.update(kwargs)
        return super(StreamURLField, self).formfield(**defaults)

south_rules = [[(StreamURLField,), [], {}]]
add_introspection_rules(south_rules, ["^streams\.fields"])