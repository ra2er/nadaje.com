import re
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.utils.encoding import smart_unicode
import urlparse


class StreamURLValidator(RegexValidator):

    regex = re.compile(
        r'^(?:http|https|rtsp|rtmp|mms)://'
        r'(?:[^\s:]*:[^\s:]*@)?'
        r'(?:(?:[A-Z0-9](?:[A-Z0-9-_.]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|' #domain...
        r'localhost|' #localhost...
        r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})' # ...or ip
        r'(?::\d+)?' # optional port
        r'(?:/?|[/?]\S+)$', re.IGNORECASE)

    def __call__(self, value):
        try:
            super(StreamURLValidator, self).__call__(value)
        except ValidationError, e:
            # Trivial case failed. Try for possible IDN domain
            if value:
                value = smart_unicode(value)
                scheme, netloc, path, query, fragment = urlparse.urlsplit(value)
                try:
                    netloc = netloc.encode('idna') # IDN -> ACE
                except UnicodeError: # invalid domain part
                    raise e
                url = urlparse.urlunsplit((scheme, netloc, path, query, fragment))
                super(StreamURLValidator, self).__call__(url)
            else:
                raise
