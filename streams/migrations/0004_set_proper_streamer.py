# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import DataMigration
from django.db import models
from streams.utils import get_stream_headers

class Migration(DataMigration):

    def forwards(self, orm):
        streams = orm.AudioStream.objects.filter(streamer='')
        print 'Parsing streams, total streams to parse: %d' % len(streams)
        counter = 1
        for stream in streams:
            try:
                headers = get_stream_headers(stream.url)
                streamer = headers.get('streamer', '')
                stream.streamer = streamer
                stream.save()
                print counter, stream.url, streamer
            except:
                print counter, stream.url, 'failed to load url'
            counter += 1


    def backwards(self, orm):
        "Write your backwards methods here."

    models = {
        'images.image': {
            'Meta': {'object_name': 'Image'},
            'height': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '500'}),
            'width': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        },
        'stations.station': {
            'Meta': {'object_name': 'Station'},
            'added_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'stations'", 'null': 'True', 'to': "orm['users.User']"}),
            'address': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '2', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'fax': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'+'", 'null': 'True', 'on_delete': 'models.SET_NULL', 'to': "orm['stations.StationImage']", 'blank': 'True', 'unique': 'True'}),
            'meta_description': ('django.db.models.fields.CharField', [], {'max_length': '160', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'postal_code': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'public': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'recommended': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50', 'blank': 'True'}),
            'subtype_attr': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'www': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'})
        },
        'stations.stationimage': {
            'Meta': {'object_name': 'StationImage', '_ormbases': ['images.Image']},
            'image_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['images.Image']", 'unique': 'True', 'primary_key': 'True'}),
            'station': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'images'", 'to': "orm['stations.Station']"})
        },
        'streams.audiostream': {
            'Meta': {'object_name': 'AudioStream', '_ormbases': ['streams.Stream']},
            'codec': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'stream_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['streams.Stream']", 'unique': 'True', 'primary_key': 'True'}),
            'streamer': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'})
        },
        'streams.stream': {
            'Meta': {'unique_together': "(('station', 'url'),)", 'object_name': 'Stream'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'bitrate': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'browser_playable': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mimetype': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'source': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'station': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'streams'", 'to': "orm['stations.Station']"}),
            'subtype_attr': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'url': ('streams.fields.StreamURLField', [], {'max_length': '200'})
        },
        'streams.videostream': {
            'Meta': {'object_name': 'VideoStream', '_ormbases': ['streams.Stream']},
            'stream_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['streams.Stream']", 'unique': 'True', 'primary_key': 'True'})
        },
        'users.user': {
            'Meta': {'object_name': 'User'},
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '75'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'stations_management': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'transmitters_management': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'})
        }
    }

    complete_apps = ['streams']
    symmetrical = True
