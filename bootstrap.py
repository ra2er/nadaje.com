import datetime
from django import forms
from django.utils.text import capfirst
from jinja2 import Markup

from templating import render_to_string, register_filter, \
    format_date, format_datetime

_missing = object()

_help_text = lambda ht, f: f.help_text if ht is _missing else ht

_widget2renderer = {
    forms.widgets.CheckboxInput:
        lambda field, inline=False, help_text=_missing: render_to_string(
            template='bootstrap/checkbox_input.html',
            context={'field': field,
                     'help_text': _help_text(help_text, field),
                     'inline': inline}),
    forms.widgets.CheckboxSelectMultiple:
        lambda field, inline=False, help_text=_missing: render_to_string(
            template='bootstrap/checkbox_select_multiple.html',
            context={'field': field,
                     'help_text': _help_text(help_text, field),
                     'inline': inline}),
    forms.widgets.RadioSelect:
        lambda field, inline=False, help_text=_missing: render_to_string(
            template='bootstrap/radio_select.html',
            context={'field': field,
                     'help_text': _help_text(help_text, field),
                     'inline': inline}),
    forms.widgets.HiddenInput:
        lambda field, help_text=_missing: render_to_string(
            template='bootstrap/hidden_input.html',
            context={'field': field,
                     'help_text': _help_text(help_text, field)}),
    forms.models.InlineForeignKeyHiddenInput:
        lambda field, help_text=_missing: render_to_string(
            template='bootstrap/hidden_input.html',
            context={'field': field,
                     'help_text': _help_text(help_text, field)}),
    forms.widgets.MultipleHiddenInput:
        lambda field, help_text=_missing: render_to_string(
            template='bootstrap/hidden_input.html',
            context={'field': field,
                     'help_text': _help_text(help_text, field)}),
}


def register(widget, renderer):
    _widget2renderer[widget] = renderer


def _form(form, display_form_errors, admin_form, *layout, **fields_config):
    fieldsets = []
    if not layout:
        fields = [f for f in form]
    else:
        fields = []
        for field in layout:
            if isinstance(field, basestring):
                fields.append(form[field])
            else:
                fieldsets.append((field[0], [form[f] for f in field[1]]))
    context = {
        'fields_config': fields_config,
        'form': form,
        'fields': fields,
        'fieldsets': fieldsets,
        'display_form_errors': display_form_errors,
    }
    return Markup(render_to_string('bootstrap/form.html', context=context))


def form(form, *layout, **fields_config):
    return _form(form, True, False, *layout, **fields_config)


def subform(form, *layout, **fields_config):
    # same as form but hide errors
    return _form(form, False, False, *layout, **fields_config)


def field(f, **kwargs):
    widget_renderer = _widget2renderer.get(
        f.field.widget.__class__,
        lambda field, help_text=_missing: render_to_string(
            template='bootstrap/field_base.html',
            context={'field': field,
                     'help_text': _help_text(help_text, field)}))
    return Markup(widget_renderer(f, **kwargs))


def value(v):
    if isinstance(v, datetime.datetime):
        return format_datetime(v)
    elif isinstance(v, datetime.date):
        return format_date(v)
    elif callable(v):
        return v()
    elif isinstance(v, bool):
        return Markup('<i class="icon-ok"></i>' if v
                      else '<i class="icon-remove"></i>')
    return unicode(v if v is not None else '')


def get_field_details(obj, field_name, **config):
    field = obj._meta.get_field(field_name)
    if hasattr(obj, 'get_%s_details' % field.name):
        field_value = getattr(obj, 'get_%s_details' % field.name)()
    else:
        field_value = field.value_from_object(obj)
    help_text = config.get('help_text', field.help_text)
    return {
        'name': field.name,
        'label': capfirst(field.verbose_name),
        'value': value(field_value),
        'help_text': help_text,
    }


def object_details(obj, *layout, **fields_config):
    details = []
    grouped_details = False

    if not layout:
        for field in obj._meta.fields:
            details.append(get_field_details(obj, field.name,
                                             **fields_config.get(field.name, {})))
    elif all(isinstance(field, basestring) for field in layout):
        for field in layout:
            details.append(get_field_details(obj, field, **fields_config.get(field, {})))
    else:
        grouped_details = True
        for fieldset in layout:
            details.append((fieldset[0], [get_field_details(obj, f, **fields_config.get(f, {})) for f in fieldset[1]]))

    return Markup(render_to_string('bootstrap/details.html', context={
        'details': details,
        'grouped_details': grouped_details,
    }))


def register_filters(env):
    register_filter(field, env=env)
    register_filter(form, env=env)
    register_filter(subform, env=env)
    register_filter(value, name='value', env=env)
    register_filter(object_details, name='object_details', env=env)
    register_filter(get_field_details, name='get_field_details', env=env)
