from django.http import HttpResponse
from httplib import BAD_REQUEST, CREATED, FOUND, OK

class AjaxResposneMiddleware(object):

    def process_response(self, request, response):
        if request.is_ajax():
            if request.method == 'POST' and response.status_code == OK:
                response.status_code = BAD_REQUEST
            elif response.status_code == FOUND:
                response = HttpResponse(content=response.get('Location'))
                response.status_code = CREATED
        return response

