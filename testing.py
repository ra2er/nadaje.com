# -*- coding: utf-8 -*-
from django.conf import settings
from django.contrib.auth import authenticate, login
from django.contrib.messages.middleware import MessageMiddleware
from django.http import HttpRequest
from django.test import TestCase, Client
from django.test.signals import template_rendered
from django.utils.importlib import import_module
from httplib import FOUND, OK
import re
from tidylib import tidy_document

import templating


class Client(Client):

    def login(self, **credentials):
        user = authenticate(**credentials)
        if user and 'django.contrib.sessions' in settings.INSTALLED_APPS:
            engine = import_module(settings.SESSION_ENGINE)

            # Create a fake request to store login details.
            request = HttpRequest()
            if self.session:
                request.session = self.session
            else:
                request.session = engine.SessionStore()
            login(request, user)

            # Save the session values.
            request.session.save()

            # Set the cookie to represent the session.
            session_cookie = settings.SESSION_COOKIE_NAME
            self.cookies[session_cookie] = request.session.session_key
            cookie_data = {
                'max-age': None,
                'path': '/',
                'domain': settings.SESSION_COOKIE_DOMAIN,
                'secure': settings.SESSION_COOKIE_SECURE or None,
                'expires': None,
            }
            self.cookies[session_cookie].update(cookie_data)
            return True
        else:
            return False


class BaseTestCase(TestCase):

    client_class = Client

    def setUp(self):
        self._original_settings = self._setup_settings({
            'DEBUG': True
        })
        super(BaseTestCase, self).setUp()

    def tearDown(self):
        self._teardown_settings(self._original_settings)
        super(BaseTestCase, self).tearDown()

    def _validate_html(self, html, template_name, extra_ignores=[]):
        document, errors = tidy_document(html, options={'numeric-entities': 1,
                                                        'drop-proprietary-attributes': 0,
                                                        'quote-ampersand': 0})
        ignores = [re.compile('^.*Warning: <form> proprietary attribute "autocomplete".*'),
                   re.compile('^.*Warning: <form> lacks "action" attribute.*'),
                   re.compile('^.*Warning: <input> proprietary attribute "placeholder".*'),
                   re.compile('^.*Warning: <input> proprietary attribute "autocomplete".*'),
                   re.compile('^.*Warning: <[^>]*> proprietary attribute "data-'),
                   re.compile('^.*Warning: trimming empty <i>'),
                   re.compile('^.*Warning: <iframe> proprietary attribute "allowtransparency"')] + extra_ignores
        errors = u'\n'.join([e for e in errors.split('\n') if e and not any(ignore.match(e) for ignore in ignores)])
        if errors:
            self.assertEqual(errors, '', (u'\n\nHTML "%s"\nvalidation error:\n%s\n\n%s' % (
                template_name,
                errors,
                u'\n'.join(u'%i:\t%s' %(n,l) for (n,l) in enumerate(html.split(u'\n'), 1)))).encode('utf-8')
            )

    # helpers
    def _setup_settings(self, custom_settings):
        original_settings = {}
        for setting_name, value in custom_settings.items():
            if hasattr(settings, setting_name):
                original_settings[setting_name] = getattr(settings,
                                                          setting_name)
            setattr(settings, setting_name, value)
        return original_settings

    def _teardown_settings(self, original_settings, custom_settings=None):
        custom_settings = custom_settings or {}
        for setting_name, value in original_settings.items():
            setattr(settings, setting_name, value)
            if setting_name in custom_settings:
                del custom_settings[setting_name]
        for setting_name, value in custom_settings.items():
            delattr(settings, setting_name)


class _MessageMiddleware(MessageMiddleware):

    def process_response(self, request, response):
        response = super(_MessageMiddleware, self).process_response(request, response)
        if hasattr(request, '_messages'):
            response._messages = request._messages
        return response


class ViewsTestCase(BaseTestCase):

    def setUp(self):
        self.original_middleware_classes = settings.MIDDLEWARE_CLASSES
        settings.MIDDLEWARE_CLASSES = \
            ['testing._MessageMiddleware'
             if m == 'django.contrib.messages.middleware.MessageMiddleware'
             else m for m in settings.MIDDLEWARE_CLASSES]
        super(ViewsTestCase, self).setUp()

    def tearDown(self):
        settings.MIDDLEWARE_CLASSES = self.original_middleware_classes
        super(ViewsTestCase, self).tearDown()

    def _test_status(self, url, method='get', *args, **kwargs):
        status_code = kwargs.pop('status_code', OK)
        client = kwargs.pop('client_instance', Client())
        skip_html_validation = kwargs.pop('skip_html_validation', False)
        data = kwargs.pop('data', {})
        follow = kwargs.pop('follow', False)

        response = getattr(client, method)(url, data=data, follow=follow, **kwargs)
        self.assertEqual(response.status_code, status_code,
            u'Incorrect status code for: %s, (data=%s, %s)! Expected: %s, received: %s.' % (
                url.decode('utf-8'), data, kwargs, status_code, response.status_code,))
        if status_code == OK and not skip_html_validation and response.templates:
            self._validate_html(response.content.decode('utf-8'), response.templates[0])
        return response

    def _test_GET_status(self, url, *args, **kwargs):
        return self._test_status(url, 'get', *args, **kwargs)

    def _test_POST_status(self, url, *args, **kwargs):
        kwargs['status_code'] = kwargs.pop('status_code', FOUND)
        check_messages = kwargs.pop('check_messages', None)
        response = self._test_status(url, 'post', *args, **kwargs)
        # by default check success messages
        check_messages = check_messages if check_messages is not None else response.status_code == FOUND
        if check_messages:
            self.assertTrue(len(response._messages) > 0)
        return response


def on_template_render(signal, *args, **kwargs):
    template_rendered.send(*args, **kwargs)

class JinjaViewsTestCase(ViewsTestCase):

    def setUp(self):
        super(JinjaViewsTestCase, self).setUp()
        templating.template_rendered.connect(on_template_render, weak=False)

    def tearDown(self):
        super(JinjaViewsTestCase, self).tearDown()
        templating.template_rendered.disconnect(on_template_render)
