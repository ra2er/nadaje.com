from django.conf import settings
from django.core.urlresolvers import reverse
from itsdangerous import TimestampSigner
from testing import JinjaViewsTestCase
from users.models import User


class ViewsTestCase(JinjaViewsTestCase):

    DEFAULT_PASSWORD = 'password'

    def setUp(self):
        super(ViewsTestCase, self).setUp()

    def test_signup(self):
        signup_url = reverse('user-signup')
        self._test_GET_status(signup_url)
        response = self._test_POST_status(signup_url, data={'email': 'test.test@example.com'})
        self.assertRedirects(response, reverse('home-page'))

    def test_signup_token(self):
        email = 'test.test@example.com'
        signer = TimestampSigner(settings.SECRET_KEY)
        token = signer.sign(email)
        register_url = reverse('user-register', args=[token])
        self._test_GET_status(register_url)
        response = self._test_POST_status(register_url, data={'password1': self.DEFAULT_PASSWORD,
                                                              'password2': self.DEFAULT_PASSWORD})
        self.assertTrue(User.objects.filter(email=email).exists())
        self.assertRedirects(response, reverse('home-page'))