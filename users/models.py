#-*- coding: utf-8 -*-
import email.utils
import random
from django.db import models
from django.utils.translation import ugettext_lazy as _
from . import utils
from stations.models import Station


class UserManager(models.Manager):

    def create_user(self, email, password, is_admin=False, is_staff=False):
        user = self.get_query_set().create(email=email, password=password,
                                           is_admin=is_admin, is_staff=is_staff or is_admin)
        user.set_password(password)
        user.save()
        return user


class User(models.Model):

    UNUSABLE_PASSWORD = '!' # This will never be a valid hash

    username = models.CharField(_('username'), unique=True, max_length=100)
    email = models.EmailField(_('e-mail address'), unique=True)
    password = models.CharField(_('password'), max_length=128)
    first_name = models.CharField(_('first name'), max_length=100, blank=True)
    last_name = models.CharField(_('last name'), max_length=100, blank=True)
    is_staff = models.BooleanField(_('is staff'), default=False)
    is_admin = models.BooleanField(_('is admin'), default=False)

    stations_management = models.BooleanField(_('stations management'), default=False)
    transmitters_management = models.BooleanField(_('transmitters management'), default=False)

    objects = UserManager()

    def __unicode__(self):
        return email.utils.formataddr(
            (' '.join(filter(None, [self.first_name,
                                    self.last_name])), self.email)
        )

    @property
    def full_name(self):
        return email.utils.formataddr(
            (' '.join(filter(None, [self.first_name,
                                    self.last_name])), self.email)
        )

    def set_password(self, raw_password):
        if raw_password is None:
            self.password = User.UNUSABLE_PASSWORD
        else:
            salt = utils.get_hexdigest(str(random.random()),
                                       str(random.random()))[:5]
            hsh = utils.get_hexdigest(salt, raw_password)
            self.password = '%s$%s' % (salt, hsh)

    def check_password(self, raw_password):
        return (self.has_usable_password() and
                utils.check_password(raw_password, self.password))

    def has_usable_password(self):
        return (self.password is not None and
                self.password != User.UNUSABLE_PASSWORD and
                '$' in self.password)


class Profile(models.Model):

    user = models.OneToOneField(User, related_name='profile')
    stations = models.ManyToManyField(Station, related_name='+')
