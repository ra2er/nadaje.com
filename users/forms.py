#-*- coding: utf-8 -*-
from __future__ import absolute_import
from django import forms
from django.contrib.auth import authenticate
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.utils.translation import ugettext_lazy as _, ugettext
from users.models import User


class AuthenticationForm(forms.Form):

    login = forms.CharField(label=_('Email or username'))
    password = forms.CharField(label=_('Password'), widget=forms.PasswordInput)

    def __init__(self, request=None, *args, **kwargs):
        self.request = request
        self.user_cache = None
        super(AuthenticationForm, self).__init__(*args, **kwargs)

    def clean(self):
        login = self.cleaned_data.get('login')
        password = self.cleaned_data.get('password')

        if login and password:
            try:
                validate_email(login)
                self.user_cache = authenticate(email=login, password=password)
            except ValidationError:
                self.user_cache = authenticate(username=login, password=password)
            if self.user_cache is None:
                raise forms.ValidationError(ugettext(
                    'Please enter a correct email and password. '
                    'Note that both fields are case-sensitive.'))
        self.check_for_test_cookie()
        return self.cleaned_data

    def check_for_test_cookie(self):
        if self.request and not self.request.session.test_cookie_worked():
            raise forms.ValidationError(
                ugettext(
                    'Your Web browser doesn\'t appear to have '
                    'cookies enabled. '
                    'Cookies are required for logging in.'))

    def get_user_id(self):
        if self.user_cache:
            return self.user_cache.id
        return None

    def get_user(self):
        return self.user_cache


class PasswordForm(forms.Form):

    password1 = forms.CharField(label=_('Password'), widget=forms.PasswordInput())
    password2 = forms.CharField(label=_('Confirm password'), widget=forms.PasswordInput())

    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
    }

    def clean_new_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2:
            if password1 != password2:
                self._errors['password2'] = self.error_class([self.error_messages['password_mismatch']])
        return password2

    def clean(self):
        cleaned_data = super(PasswordForm, self).clean()
        self.clean_new_password2()
        return cleaned_data


class SignupForm(forms.Form):

    email = forms.EmailField(label=_('E-mail'))

    def clean(self):
        cleaned_data = super(SignupForm, self).clean()
        email = cleaned_data.get('email')
        if email and User.objects.filter(email__iexact=email):
            raise forms.ValidationError(_('Given email is already taken.'))
        return cleaned_data


class EmailForm(forms.Form):

    email = forms.EmailField(label=_('E-mail'))


class PasswordResetTokenForm(EmailForm):

    def clean(self):
        cleaned_data = super(PasswordResetTokenForm, self).clean()
        email = cleaned_data.get('email')
        if email and not User.objects.filter(email=email).exists():
            raise forms.ValidationError(_('Email not found in our database.'))
        return cleaned_data