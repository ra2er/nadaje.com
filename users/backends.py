from . import models
from django.db.models import Q


class ModelBackend(object):

    supports_object_permissions = False
    supports_anonymous_user = False
    supports_inactive_user = False

    def authenticate(self, username=None, email=None, password=None):
        try:
            user = models.User.objects.get(Q(username=username) | Q(email=email))
            if user.check_password(password):
                return user
        except models.User.DoesNotExist:
            return None

    def get_user(self, user_id):
        try:
            return models.User.objects.get(pk=user_id)
        except models.User.DoesNotExist:
            return None