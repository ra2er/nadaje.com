from django.conf.urls import url, patterns

from . import views

urlpatterns = patterns('',
    url(r'^login/$', views.login, name='user-login'),
    url(r'^logout/$', views.logout, name='user-logout'),
    url(r'^signup/$', views.signup, name='user-signup'),
    url(r'^register/(?P<token>(.*))/$', views.register, name='user-register'),
    url(r'^reset-password-request/$', views.passwd_reset_request, name='passwd-reset-request'),
    url(r'^reset-password/(?P<token>(.*))/$', views.reset_passwd, name='user-passwd-reset'),

)