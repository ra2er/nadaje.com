# -*- coding: utf-8 -*-
from __future__ import absolute_import
import urlparse
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import REDIRECT_FIELD_NAME, login as auth_login, \
    logout as auth_logout, authenticate
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, get_object_or_404
from django.utils.translation import ugettext
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect

from . import forms
from . import decorators
from itsdangerous import TimestampSigner, SignatureExpired, BadSignature
from templating import render_to_response
from users.models import User, Profile
from utils.mails import prepare_and_send


@csrf_protect
@never_cache
def login(request):
    if request.user is not None:
        return redirect(reverse('home-page'))
    redirect_to = request.REQUEST.get(REDIRECT_FIELD_NAME, '')

    form = forms.AuthenticationForm(request=request, data=request.POST or None)
    if form.is_valid():
        netloc = urlparse.urlparse(redirect_to)[1]

        user = form.get_user()
        # Okay, security checks complete. Log the user in.
        auth_login(request, user)

        # Use default setting if redirect_to is empty
        if not redirect_to or netloc and netloc != request.get_host():
            if user.is_admin:
                redirect_to = reverse('admin')
            else:
                redirect_to = reverse('home-page')

        if request.session.test_cookie_worked():
            request.session.delete_test_cookie()

        return HttpResponseRedirect(redirect_to)
    request.session.set_test_cookie()

    return render_to_response('users/login.html', context={'form': form},
                              request=request)


def logout(request):
    auth_logout(request)
    redirect_to = request.GET.get('next', unicode(decorators.LOGIN_URL))
    return HttpResponseRedirect(redirect_to)


@csrf_protect
@never_cache
def signup(request):
    form = forms.SignupForm(data=request.POST or None)
    if form.is_valid():
        signer = TimestampSigner(settings.SECRET_KEY+'signup')
        email = form.cleaned_data['email']
        token = signer.sign(email)
        prepare_and_send('signup', emails=[email], extra_context={
            'token': urlparse.urlunparse(('http', settings.DOMAIN,
                                          reverse('user-register', args=[token]), '', '', ''))
        })
        messages.success(request, ugettext('An email message has been sent to %s, '
                                           'to complete registration process please '
                                           'follow instructions contained in email.' % email))
        return redirect(reverse('home-page'))
    context = {'form': form}
    return render_to_response('users/signup.html', request=request, context=context)


def register(request, token):
    redirect_to = reverse('home-page')
    try:
        signer = TimestampSigner(settings.SECRET_KEY+'signup')
        email = signer.unsign(token, max_age=settings.SIGNUP_TOKEN_LIFE)
        if User.objects.filter(email__iexact=email):
            messages.error(request, ugettext('This token has been used already.'))
            return redirect(redirect_to)
        form = forms.PasswordForm(data=request.POST or None)
        if form.is_valid():
            password = form.cleaned_data['password1']
            u = User.objects.create_user(email, password)
            Profile.objects.create(user=u)
            messages.success(request, ugettext('Your account is created and ready for use.'))
            user = authenticate(email=u.email, password=password)
            auth_login(request, user)
            return redirect(redirect_to)
        context = {'form': form}
        return render_to_response('users/register.html', request=request, context=context)
    except SignatureExpired:
        messages.error(request, ugettext('Signup token has expired.'))
        return redirect(redirect_to)
    except BadSignature:
        messages.error(request, ugettext('Signup token is invalid.'))
        return redirect(redirect_to)


def passwd_reset_request(request):
    form = forms.PasswordResetTokenForm(data=request.POST or None)
    if form.is_valid():
        signer = TimestampSigner(settings.SECRET_KEY+'reset-password')
        email = form.cleaned_data['email']
        token = signer.sign(email)
        prepare_and_send('password_reset', emails=[email], extra_context={
            'token': urlparse.urlunparse(('http', settings.DOMAIN,
                                          reverse('user-passwd-reset', args=[token]), '', '', ''))
        })
        messages.success(request, ugettext('An email message has been sent to %s, '
                                           'to complete password reset process please '
                                           'follow instructions contained in email.' % email))
        return redirect(reverse('home-page'))
    context = {'form': form}
    return render_to_response('users/password_reset_request.html', request=request, context=context)


def reset_passwd(request, token):
    redirect_to = reverse('home-page')
    try:
        signer = TimestampSigner(settings.SECRET_KEY+'reset-password')
        email = signer.unsign(token, max_age=settings.SIGNUP_TOKEN_LIFE)
        form = forms.PasswordForm(data=request.POST or None)
        u = get_object_or_404(User, email=email)
        if form.is_valid():
            password = form.cleaned_data['password1']
            u.set_password(password)
            u.save()
            messages.success(request, ugettext('Your password has been successfully changed, you can now log in with new password.'))
            return redirect(redirect_to)
        context = {'form': form}
        return render_to_response('users/password_reset.html', request=request, context=context)
    except SignatureExpired:
        messages.error(request, ugettext('Signup token has expired.'))
        return redirect(redirect_to)
    except BadSignature:
        messages.error(request, ugettext('Signup token is invalid.'))
        return redirect(redirect_to)