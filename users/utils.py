from django.utils.crypto import constant_time_compare
from django.utils.encoding import smart_str
from django.utils.hashcompat import sha_constructor


def get_hexdigest(salt, raw_password):
    raw_password, salt = smart_str(raw_password), smart_str(salt)
    return sha_constructor(salt + raw_password).hexdigest()

def check_password(raw_password, enc_password):
    salt, hsh = enc_password.split('$')
    return constant_time_compare(hsh, get_hexdigest(salt, raw_password))
