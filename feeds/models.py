#-*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _

from utils import utcnow


class Article(models.Model):

    caption = models.CharField(_('caption'), max_length=200)
    content = models.TextField(_('content'))
    pub_date = models.DateTimeField(default=utcnow)


class Feed(models.Model):

    source = models.CharField(_('source'), max_length=100)
    caption = models.CharField(_('caption'), max_length=255)
    url = models.URLField(_('link'))
    content = models.TextField(_('content'))
    pub_date = models.DateTimeField(default=utcnow)