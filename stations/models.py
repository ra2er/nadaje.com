#-*- coding: utf-8 -*-
from __future__ import absolute_import
from django.db import models
from django.utils.translation import ugettext_lazy as _

from utils import countries
from utils.db.models import Subtyped
from utils.images.models import Image


class AddressMixin(models.Model):

    country = models.CharField(_('country'), max_length=2, blank=True,
                               choices=countries.COUNTRY_CHOICES)
    city = models.CharField(_('city'), max_length=100, blank=True)
    address = models.CharField(_('address'), max_length=200, blank=True)
    postal_code = models.CharField(_('postal code'), max_length=10, blank=True)

    class Meta:
        abstract = True

    @property
    def full_address(self):
        return ', '.join([self.address, '%s %s' % (self.postal_code, self.city), self.get_country_display()])


class Group(Subtyped):

    name = models.CharField(_('name'), max_length=100)
    slug = models.SlugField(unique=True)
    description = models.TextField(_('description'), blank=True)

    def __unicode__(self):
        return self.name


class RadioGroup(Group):

    stations = models.ManyToManyField('RadioStation', related_name='groups')


class Station(Subtyped, AddressMixin):

    name = models.CharField(_('name'), max_length=100)
    slug = models.SlugField(editable=False, blank=True)
    description = models.TextField(_('description'), blank=True)
    email = models.EmailField(_('email'), blank=True)
    phone = models.CharField(_('phone'), max_length=100, blank=True)
    fax = models.CharField(_('phone'), max_length=100, blank=True)
    www = models.URLField(_('www'), blank=True)

    logo = models.OneToOneField('StationImage', related_name='+', null=True,
                                blank=True, on_delete=models.SET_NULL)
    recommended = models.BooleanField(_('recommended'), default=False)
    public = models.BooleanField(_('public'))
    added_by = models.ForeignKey('users.User', null=True, related_name='stations')
    meta_description = models.CharField(_('meta description'),
                                        blank=True, max_length=160)

    def __unicode__(self):
        return u'%s' % self.name

    @property
    def station_type(self):
        raise NotImplementedError()

    def get_streams(self, mobile=False):
        codecs = ['mp3', 'ogg', 'aacp']
        def comparison(codec):
            try:
                return codecs.index(codec)
            except ValueError:
                return float('inf')
        streams = self.streams.filter(active=True)
        if mobile:
            streams = streams.exclude(codec='aacp')
        streams = [s.get_subtype_instance() for s in streams]
        return sorted(streams, key=lambda s: comparison(s.codec))

    @property
    def has_streams(self):
        return self.streams.filter(active=True).count > 0

    def get_meta_description(self, length=160):

        default_meta_description = _('%(station)s  Online. FM radio via the Internet. '
                                     'How to listen: Winamp, Windows Media Player, Real Player. '
                                     'Plish and foreign Internet radio stations.' % {
            'station': self.name
        })
        meta_description = self.meta_description or self.description or default_meta_description
        if len(meta_description) <= length:
            return meta_description
        words = meta_description.split(' ')
        result = []
        m = 0
        for word in words:
            m += len(word) + 1
            if m > length:
                break
            result.append(word)
        return u' '.join(result)


class RadioStation(Station):

    typ = models.CharField(_('type'), max_length=5,
                           choices=[('FM', _('FM')),
                                    ('DAB', _('Digital')),
                                    ('INET', _('Internet'))])
    genres = models.ManyToManyField('Genre', related_name='radios',
                                    null=True, blank=True)

    @property
    def station_type(self):
        return 'radio'


class TVStation(Station):

    @property
    def station_type(self):
        return 'tv'


class Genre(models.Model):

    name = models.CharField(_('name'), max_length=50)
    slug = models.SlugField(unique=True)

    def __unicode__(self):
        return self.name


class StationImage(Image):

    station = models.ForeignKey(Station, related_name='images')
