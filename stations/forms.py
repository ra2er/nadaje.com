from __future__ import absolute_import
from django import forms
from django.db.models import Q
from django.db.models.query import EmptyQuerySet
from django.utils.translation import ugettext_lazy as _, ugettext
from . import models
from users.models import User
from utils.countries import COUNTRY_CHOICES
from utils.forms import SelfAwareFormMixin, QueryField


class StationFormBase(forms.ModelForm):

    class Meta:
        exclude = ['logo']

    def __init__(self, *args, **kwargs):
        super(StationFormBase, self).__init__(*args, **kwargs)
        self.fields['added_by'] = forms.IntegerField(label=_('Added by'))
        self.fields['added_by'].initial = self.instance.added_by.pk

    def clean(self):
        cleaned_data = super(StationFormBase, self).clean()
        name = cleaned_data.get('name')
        if not self.instance.pk and name and type(self.instance).objects.filter(name__iexact=name).exists():
            raise forms.ValidationError(_('Station %s already exists.' % name))
        user_pk = cleaned_data.get('added_by')
        try:
            cleaned_data['added_by'] = User.objects.get(pk=user_pk)
        except User.DoesNotExist:
            self.fields['added_by'].errors = self.error_class([_('Invalid user')])
        return cleaned_data


class StationTypeSelectForm(forms.Form):

    typ = forms.ChoiceField(label=_('Station type'),
                            choices=[('radio', _('Radio station')),
                                     #('tv', _('TV station'))])
    ])


class StationImageForm(forms.ModelForm):

    set_as_logo = forms.BooleanField(label=_('Set as logo'), required=False)

    def __init__(self, *args, **kwargs):
        super(StationImageForm, self).__init__(*args, **kwargs)
        if self.instance.station.logo == self.instance:
            self.fields.pop('set_as_logo')

    class Meta:
        model = models.StationImage
        exclude = ['station']


class StationFilterForm(SelfAwareFormMixin):

    query = QueryField(label='&nbsp;', required=False, help_text='',
                       widget=forms.TextInput(attrs={'placeholder':
                                                         _('station name')}))
    country = forms.ChoiceField(label='&nbsp;', required=False, help_text='',
                                initial='all')

    def __init__(self, stations, use_countries=False, *args, **kwargs):
        self.stations = stations
        super(StationFilterForm, self).__init__(*args, **kwargs)
        if use_countries:
            countries_list = [v[0] for v in
                              self.stations.values_list('country').distinct()]
            countries = dict(COUNTRY_CHOICES)
            country_choices = [('all', _('All countries'))]
            for country_code in countries_list:
                if not country_code:
                    continue
                country_choices.append((country_code, countries[country_code]))
            country_choices = sorted(country_choices,
                                     key=lambda country_choice: country_choice[1])
            self.fields['country'].choices = country_choices
        else:
            del self.fields['country']

    def search(self):
        stations = self.stations
        parts = self.cleaned_data.get('query')
        if parts:
            query = Q()
            for part in parts:
                query &= (Q(name__icontains=part))
            stations = stations.filter(query)
        country = self.cleaned_data.get('country')
        if country and not country == 'all':
            stations = stations.filter(country=country)
        return stations.distinct()


class StationSearchForm(SelfAwareFormMixin):

    query = QueryField(label='&nbsp;', required=False, help_text='',
                       widget=forms.TextInput(attrs={'placeholder':
                                                         _('search for stations'),
                                                     'autocomplete': 'off'}))

    def search(self):
        stations = (models.RadioStation.objects.filter(public=True)
                    if self.cleaned_data.get('query')
                    else models.RadioStation.objects.none())
        parts = self.cleaned_data.get('query')
        if parts:
            query = Q()
            for part in parts:
                query &= (Q(name__icontains=part))
            stations = stations.filter(query)
        country = self.cleaned_data.get('country')
        if country and not country == 'all':
            stations = stations.filter(country=country)
        return stations.distinct()