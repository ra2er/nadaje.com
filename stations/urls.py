from django.conf.urls import patterns, include, url
from . import views

radio_stations_patterns = patterns('',
    url(r'^(?P<typ>(FM|INET|DAB))/$', views.homeland_radio_stations_index,
        name='station-index'),
    url(r'^(?P<typ>(FM|INET|DAB))/foreign/$', views.foreign_radio_stations_index,
        name='foreign-station-index'),
    url(r'^genre/(?P<genre_pk>\d+)/$',
        views.radio_stations_by_genre, name='station-genre-index'),
    url(r'^station/(?P<station_pk>\d+)/$',
        views.radio_station_details, name='station-details'),
    url(r'^station/(?P<station_pk>\d+)/edit/$',
        views.radio_station_edit, name='station-edit'),
    url(r'^station/(?P<station_pk>\d+)/add-to-favourites/$',
        views.station_add_to_fav, name='station-add-to-fav'),
    url(r'^station/(?P<station_pk>\d+)/remove-from-favourites/$',
        views.station_remove_from_fav, name='station-remove-from-fav'),
    url(r'^station/add/$', views.radio_station_add, name='station-add'),
    url(r'^groups/$', views.radio_group_index, name='radio-group-index'),
    url(r'^groups/group/(?P<group_pk>\d+)/$',
        views.radio_group_details, name='radio-group-details'),
    url(r'^radio-player/station/(?P<station_pk>\d+)/stream/(?P<stream_pk>\d+)/$',
        views.radio_player, name='player'),
    url(r'^radio-player/stream/(?P<stream_pk>\d+)/$',
        views.play_stream, name='stream')
)

tv_stations_patterns = patterns('',
    url(r'^$', views.tv_station_index, name='index'),
    url(r'^d$', views.d)
)

urlpatterns = patterns('',
    url(r'^radio-stations/', include(radio_stations_patterns,
                                     namespace='radio-stations')),
    url(r'^tv-stations/', include(tv_stations_patterns,
                                  namespace='tv-stations')),
    url(r'^search/$', views.station_search, name='station-search')
)
