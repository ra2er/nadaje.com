from django.core.urlresolvers import reverse
from stations.models import RadioStation
from testing import JinjaViewsTestCase, Client
from users.models import User, Profile


class ViewsTestCase(JinjaViewsTestCase):

    DEFAULT_PASSWORD = 'password'

    def setUp(self):
        super(ViewsTestCase, self).setUp()

        self.station = RadioStation.objects.create(name='test', typ='FM', public=True)
        self.user = User.objects.create_user(email='user@example.com',
                                                   password=self.DEFAULT_PASSWORD,
                                                   is_admin=False)
        self.user_client = Client()
        self.user_client.login(email=self.user.email,
                               password=self.DEFAULT_PASSWORD)
        self.user_profile = Profile.objects.create(user=self.user)

    def test_station_index(self):
        station_index_url = reverse('radio-stations:station-index', args=['FM'])
        self._test_GET_status(station_index_url, client_instance=self.user_client)

    def test_station_details(self):
        station_details_url = reverse('radio-stations:station-details', args=[self.station.pk])
        self._test_GET_status(station_details_url, client_instance=self.user_client)

    def test_station_add_to_fav(self):
        add_to_fav_url = reverse('radio-stations:station-add-to-fav', args=[self.station.pk])
        redirect_to = reverse('radio-stations:station-details', args=[self.station.pk])
        response = self._test_POST_status(add_to_fav_url, client_instance=self.user_client, data={
            'redirect_to': redirect_to
        })
        profile = self.user.profile
        station = RadioStation.objects.get()
        self.assertTrue(profile.stations.filter(pk=station.pk).exists())
        self.assertRedirects(response, redirect_to)

    def test_remove_station_from_fav(self):
        profile = self.user.profile
        station = RadioStation.objects.get()
        profile.stations.add(station)
        remove_from_fav_url = reverse('radio-stations:station-remove-from-fav', args=[self.station.pk])
        redirect_to = reverse('radio-stations:station-details', args=[self.station.pk])
        response = self._test_POST_status(remove_from_fav_url, client_instance=self.user_client, data={
            'redirect_to': redirect_to
        })
        self.assertFalse(profile.stations.filter(pk=station.pk).exists())
        self.assertRedirects(response, redirect_to)