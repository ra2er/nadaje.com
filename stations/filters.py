#-*- coding: utf-8 -*-
from templating import register_filter


def get_first_playable_stream(station):
    streams = station.streams.all()
    for s in streams:
        s = s.get_subtype_instance()
        if s.browser_playable:
            return s
    return None


def register_filters(env):
    register_filter(get_first_playable_stream, name='station.playable_stream')