#-*- coding: utf-8 -*-
import os
import urlparse
from django.conf import settings
from django.contrib import messages
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.db.models import Q, Count
from django.forms.models import modelform_factory, modelformset_factory, inlineformset_factory
from django.http import Http404, HttpResponse
from django.shortcuts import get_object_or_404, redirect
from django.utils import simplejson
from django.utils.translation import ugettext
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST
from logs.signals import station_add

from stations import models, forms
from streams.forms import AudioStreamForm, StreamInlineFormset
from streams.models import Stream, AudioStream
from templating import render_to_response
from transmitters.models import TransmitterLocation
from users.decorators import login_required
from utils import slugify
from utils.mails import prepare_and_send


def radio_station_index(request, stations, context, template):
    context.update(stations=stations)
    return render_to_response(template, request=request, context=context)


def homeland_radio_stations_index(request, typ):
    country = 'PL'
    stations = models.RadioStation.objects.filter(country=country,
                                                  typ=typ, public=True)
    context = {'country': country, 'typ': typ}
    filter_form = forms.StationFilterForm(data=request.GET,
                                          stations=stations)
    if filter_form.is_valid():
        stations = filter_form.search()
    context.update(filter_form=filter_form)
    return radio_station_index(request, stations=stations, context=context,
                               template='stations/homeland_radio_station_index.html')


def foreign_radio_stations_index(request, typ):
    stations = models.RadioStation.objects.exclude(country='PL').filter(typ=typ, public=True)
    filter_form = forms.StationFilterForm(data=request.GET, use_countries=True,
                                          stations=stations)
    context = {'typ': typ}
    if filter_form.is_valid():
        stations = filter_form.search()
    context.update(filter_form=filter_form)
    return radio_station_index(request, stations=stations, context=context,
                               template='stations/foreign_radio_station_index.html')


def radio_stations_by_genre(request, genre_pk):
    stations = models.RadioStation.objects.filter(public=True)
    genre = get_object_or_404(models.Genre, pk=genre_pk)
    stations = stations.filter(genres=genre).distinct()
    filter_form = forms.StationFilterForm(data=request.GET,
                                          stations=stations)
    if filter_form.is_valid():
        stations = filter_form.search()
    context = {
        'stations': stations,
        'filter_form': filter_form,
        'genre': genre
    }
    return radio_station_index(request, stations=stations, context=context,
                               template='stations/radio_station_index.html')


def radio_station_details(request, station_pk, country=None):
    station = get_object_or_404(models.RadioStation,
                                pk=station_pk, public=True).get_subtype_instance()
    transmitters_locations = TransmitterLocation.objects.filter(
        transmitters__radiotransmitter__station=station).distinct()
    fm_transmitters_locations = transmitters_locations.filter(transmitters__radiotransmitter__fmtransmitter__isnull=False)
    dab_transmitters_locations = transmitters_locations.filter(transmitters__radiotransmitter__dabtransmitter__isnull=False)
    context = {'station': station, 'request': request,
               'fm_transmitters_locations': fm_transmitters_locations,
               'dab_transmitters_locations': dab_transmitters_locations,
               'typ': station.typ}
    template = ('stations/radio_station_details.html'
                if isinstance(station, models.RadioStation)
                else 'stations/tv_station_details.html')
    return render_to_response(template, request=request, context=context)


def radio_group_index(request, typ='FM'):
    radio_groups = models.RadioGroup.objects.values_list('name', 'pk').annotate(
        count=Count('stations'))
    context = {
        'groups': radio_groups,
        'typ': typ
    }
    return render_to_response('stations/radio_groups.html',
                              request=request, context=context)


def radio_group_details(request, group_pk, typ='FM'):
    group = get_object_or_404(models.RadioGroup, pk=group_pk)
    context = {
        'group': group,
        'typ': typ
    }
    return render_to_response('stations/radio_group_details.html',
                              request=request, context=context)


@login_required
@require_POST
@csrf_exempt
def station_add_to_fav(request, station_pk):
    station = get_object_or_404(models.Station, pk=station_pk, public=True)
    redirect_to = request.POST.get('redirect_to',
                                   reverse('radio-stations:station-details',
                                           args=[station.pk]))
    request.user.profile.stations.add(station)
    messages.success(request,
                     ugettext(u'Station %s successfully '
                              u'added to favourites.' % station.name))
    return redirect(redirect_to)

@login_required
@require_POST
@csrf_exempt
def station_remove_from_fav(request, station_pk):
    station = get_object_or_404(models.Station, pk=station_pk, public=True)
    redirect_to = request.POST.get('redirect_to',
                                   reverse('radio-stations:station-details',
                                           args=[station.pk]))
    request.user.profile.stations.remove(station)
    messages.success(request,
                     ugettext(u'Station %s successfully '
                              u'removed from favourites.' % station.name))
    return redirect(redirect_to)


def radio_player(request, station_pk, stream_pk):
    station = get_object_or_404(models.RadioStation, pk=station_pk, public=True)
    playing = get_object_or_404(Stream, pk=stream_pk).get_subtype_instance()
    context = {'station': station, 'playing': playing}
    return render_to_response('stations/radio_player.html',
                              request=request, context=context)


def play_stream(request, stream_pk):
    stream = get_object_or_404(Stream, pk=stream_pk).get_subtype_instance()
    html = '#EXTM3U\n%s' % (stream.url)
    response = HttpResponse(html, content_type='audio/mpegurl')
    response['Content-Disposition'] = 'attachment; filename=%s.m3u' % slugify(stream.station.name)
    return response


def station_search(request):
    stations = models.RadioStation.objects.filter(public=True)
    form = forms.StationSearchForm(data=request.GET)
    if form.is_valid():
        stations = form.search()
    context = {
        'search_form': form,
        'stations': stations,
        'genres': models.Genre.objects.all(),
    }
    if request.is_ajax():
        stations = [
            {'name': station.name,
             'url': reverse('radio-stations:station-details',
                            args=[station.pk]),
             'thumbnail_url': (station.logo.get_absolute_url('frontend-thumbnail-small')
                               if station.logo else
                               os.path.join(settings.STATIC_URL, 'img/no-logo-small.png'))
            } for station in stations[:10]
        ]
        return HttpResponse(simplejson.dumps(stations))
    return render_to_response('stations/station_search.html',
                              request=request, context=context)


def tv_station_index(request):
    return render_to_response('stations/tv_station_index.html', request=request)

@login_required
def radio_station_add(request):
    station = models.RadioStation(public=True, typ='INET', added_by=request.user)
    StationForm = modelform_factory(models.RadioStation,
                                    exclude=['recommended', 'public', 'typ', 'logo', 'added_by'])
    station_form = StationForm(data=request.POST or None, instance=station)
    ImageForm = modelform_factory(models.StationImage, exclude=['station'])
    image_form = ImageForm(data=request.POST or None, files=request.FILES or None,
                           instance=models.StationImage())
    Formset = inlineformset_factory(models.RadioStation, AudioStream,
                                    fields=['url'], extra=1, form=AudioStreamForm, formset=StreamInlineFormset)
    formset = Formset(data=request.POST or None, instance=station)
    redirect_to = reverse('radio-stations:station-index', args=['INET'])
    if station_form.is_valid() and image_form.is_valid() and formset.is_valid():
        station = station_form.save()
        formset = Formset(data=request.POST or None, instance=station)
        formset.full_clean()
        formset.save()
        #image_form = ImageForm(request.POST, request.FILES,
        #                       instance=models.StationImage())
        image_form.full_clean()
        image = image_form.save(commit=False)
        image.station = station
        image.save()
        station.logo = image
        station.save()
        station_add.send(station=station, author=request.user, desc1='',
                         desc2=station_form.cleaned_data['description'], sender=models.Station)
        messages.success(request, ugettext('Station created successfully. '
                                           'You can now see your station '
                                           '<a href="%(url)s">here</a>.' % {'url': reverse('radio-stations:station-details', args=[station.pk])}))
        prepare_and_send('new_station', emails=[e for n, e in settings.MANAGERS], extra_context={
            'station': station,
            'station_url': urlparse.urlunparse(('http', settings.DOMAIN,
                                                reverse('admin:station-details', args=[station.pk]), '', '', ''))
        })
        return redirect(redirect_to)
    cancel_url = redirect_to
    context = {
        'cancel_url': cancel_url,
        'station_form': station_form,
        'formset': formset,
        'image_form': image_form
    }
    return render_to_response('stations/radio_station_add.html',
                              request=request, context=context)


@login_required
def radio_station_edit(request, station_pk):
    station = get_object_or_404(models.RadioStation, pk=station_pk)
    if station.added_by != request.user and not request.user.is_admin:
        raise PermissionDenied()
    StationForm = modelform_factory(models.RadioStation,
                                    exclude=['recommended', 'public', 'typ', 'logo', 'added_by'])
    station_form = StationForm(data=request.POST or None, instance=station)
    ImageForm = modelform_factory(models.StationImage, exclude=['station'])
    image_form = ImageForm(data=request.POST or None, files=request.FILES or None,
                           instance=station.logo)
    Formset = inlineformset_factory(models.RadioStation, AudioStream,
                                    fields=['url'], extra=1, form=AudioStreamForm, formset=StreamInlineFormset)
    formset = Formset(data=request.POST or None, instance=station)
    redirect_to = reverse('radio-stations:station-details', args=[station.pk])
    if station_form.is_valid() and image_form.is_valid() and formset.is_valid():
        station = station_form.save()
        formset = Formset(data=request.POST or None, instance=station)
        formset.full_clean()
        formset.save()
        image_form.full_clean()
        image = image_form.save(commit=False)
        image.station = station
        image.save()
        station.logo = image
        station.save()
        station_add.send(station=station, author=request.user, desc1='',
                         desc2=station_form.cleaned_data['description'], sender=models.Station)
        messages.success(request, ugettext('Station edited successfully.'))
        return redirect(redirect_to)
    cancel_url = redirect_to
    context = {
        'cancel_url': cancel_url,
        'station_form': station_form,
        'formset': formset,
        'image_form': image_form
    }
    return render_to_response('stations/radio_station_edit.html',
                              request=request, context=context)

def d(request):
    s = models.RadioStation.objects.filter(typ='FM')
    i = models.RadioStation.objects.filter(typ='INET')
    context = {'s': s, 'inet': i}
    return render_to_response('stations/d.html',
                              request=request, context=context)

