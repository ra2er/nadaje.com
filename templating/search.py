import re
from jinja2 import Markup


def prefix_marker(value, prefix, ignore_case=True):
    if not prefix:
        return value
    value = value if isinstance(value, basestring) else unicode(value)
    flags = re.IGNORECASE if ignore_case else 0
    pattern = re.compile(u'^(%s)' % prefix, flags=flags)
    return Markup(pattern.sub(ur'<strong>\1</strong>', value))


def words_marker(value, words, ignore_case=True):
    if not words:
        return value
    value = value if isinstance(value, basestring) else unicode(value)
    subpatterns = [p for w in words for p in (u'^%s'%w, u' %s'%w)]
    flags = re.IGNORECASE if ignore_case else 0
    pattern = re.compile(ur'(%s)' % u'|'.join(subpatterns), flags=flags)
    return Markup(pattern.sub(ur'<strong>\1</strong>', value))


def substring_marker(value, words, ignore_case=True):
    if not words:
        return value
    value = value if isinstance(value, basestring) else unicode(value)
    subpatterns = [u'%s'%w for w in words]
    flags = re.IGNORECASE if ignore_case else 0
    pattern = re.compile(ur'(%s)' % u'|'.join(subpatterns), flags=flags)
    return Markup(pattern.sub(ur'<strong>\1</strong>', value))
