from __future__ import absolute_import
import inspect
from itertools import chain
import os
import urllib
import yaml
from django.core.urlresolvers import reverse
import django.template.defaultfilters
from django.conf import settings
from django.dispatch import Signal
from django.http import HttpResponse, HttpResponseNotFound, \
    HttpResponseServerError, HttpResponseForbidden
from django.template import TemplateDoesNotExist
from django.template.defaultfilters import slugify
from django.template.context import get_standard_processors
from django.utils.http import urlencode
from django.utils.translation import ungettext, ugettext

from jinja2 import Environment, FileSystemLoader, Template, TemplateNotFound,\
    contextfunction, Markup, Undefined
import jinja2.ext
from stations.models import Genre
from templating import pagination, sorting, search

_jinja_env = None


def get_env():
    global _jinja_env
    if _jinja_env is None:
        _jinja_env = create_env()
    return _jinja_env


@contextfunction
def csrftoken(context):
    csrf_token = context['csrf_token']
    return Markup('<input type="hidden" name="csrfmiddlewaretoken" value="%s" />' % csrf_token)


def google_ads(ad_key, device='desktop'):
    ads_file = open(os.path.join(settings.PROJECT_ROOT, 'static', 'google_ads.yaml'), 'r')
    google_ads_config = yaml.load(ads_file.read())
    client_id = google_ads_config['client_id']
    ad = google_ads_config['ads'][ad_key]
    if device=='desktop':
        return Markup("""<script type="text/javascript"><!--
            google_ad_client = "ca-pub-%(client_id)s";
            /* new.nadaje.com jednostka linkow */
            google_ad_slot = "%(slot)s";
            google_ad_width = %(width)s;
            google_ad_height = %(height)s;
            //-->
        </script>
        <script type="text/javascript"
            src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>""" % {'client_id': client_id, 'slot': ad['slot'],
                        'width': ad['width'], 'height': ad['height']})
    elif device=='tablet':
        return Markup("""<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- nadaje-responsive -->
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-client="ca-pub-1427823302483414"
                     data-ad-slot="1137143325"
                     data-ad-format="auto"></ins>
                <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
                </script>""")
    elif device=='phone':
        return Markup("""<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- nadaje-responsive -->
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-client="ca-pub-1427823302483414"
                     data-ad-slot="1137143325"
                     data-ad-format="auto"></ins>
                <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
                </script>""")

def empty_default(value):
    if not value or isinstance(value, Undefined):
        return u'-'
    return value


def format_date(value):
    return django.template.defaultfilters.date(value, 'SHORT_DATE_FORMAT')


def format_datetime(value):
    return django.template.defaultfilters.date(value, 'SHORT_DATETIME_FORMAT')


def create_env(**extra_options):
    searchpath = list(settings.JINJA2_TEMPLATE_DIRS)
    searchpath.append(os.path.join(os.path.dirname(__file__), 'templates'))
    env = Environment(loader=FileSystemLoader(searchpath),
                      auto_reload=settings.DEBUG,
                      extensions=[jinja2.ext.i18n, jinja2.ext.with_],
                      cache_size=getattr(settings, 'JINJA2_CACHE_SIZE', 50),
                      autoescape=True, **extra_options)
    env.install_gettext_callables(gettext=ugettext, ngettext=ungettext)
    env.globals['csrftoken'] = csrftoken
    env.globals['google_ads'] = google_ads
    env.globals['search_genres'] = Genre.objects.all()
    env.globals['reverse'] = reverse
    env.globals['unicode'] = unicode
    env.globals['urlencode'] = lambda d, doseq=True: urlencode(d, doseq=doseq)
    env.globals['min'] = min
    env.globals['max'] = max
    env.globals['len'] = len
    env.globals['chain'] = lambda *args: list(chain(*args))
    env.globals['DEBUG'] = settings.DEBUG
    env.globals['STATIC_URL'] = settings.STATIC_URL
    env.globals['get_pagination'] = pagination.pagination
    env.globals['get_sorting'] = sorting.get_sorting
    env.globals['get_list_sorting'] = sorting.get_list_sorting
    env.filters['with_lower'] = sorting.with_lower
    env.filters['words_marker'] = search.words_marker
    env.filters['substring_marker'] = search.substring_marker
    env.filters['prefix_marker'] = search.prefix_marker
    env.filters['slugify'] = slugify
    env.filters['datetime'] = format_datetime
    return env


def get_template(template, globals=None, env=None):
    env = env or get_env()
    if isinstance(template, Template):
        return template
    elif isinstance(template, basestring):
        try:
            return env.get_template(template, globals=globals)
        except TemplateNotFound, e:
            raise TemplateDoesNotExist(str(e))
    for t in template:
        try:
            return env.get_template(t, globals=globals)
        except TemplateNotFound:
            continue
    raise TemplateDoesNotExist(template)


def render_to_string(template, context=None, request=None,
                     processors=None):
    template = get_template(template)
    context = dict(context or {})
    if request is not None:
        context['request'] = request
        for processor in chain(get_standard_processors(), processors or ()):
            context.update(processor(request))
    return template.render(context)


# used for testing
template_rendered = Signal(providing_args=['template', 'context'])


def render_to_response(template_name, context=None, request=None,
                       processors=None, mimetype=None):
    template = get_template(template_name)
    template_rendered.send(sender=None, template=template, context=context)
    return HttpResponse(render_to_string(template, context, request,
                                         processors), mimetype=mimetype)


def register_filter(filter_func, name=None, env=None):
    env = env or get_env()
    name = name or '%s.%s' % (inspect.getmodule(filter_func).__name__,
                              filter_func.func_name)
    env.filters[name] = filter_func


def register_global(obj, name, env=None):
    assert '.' not in name
    env = env or get_env()
    env.globals[name] = obj


def page_not_found(request):
    return HttpResponseNotFound(render_to_string('404.html',
                                                 request=request))


def server_error(request):
    return HttpResponseServerError(render_to_string('500.html',
                                                    request=request))


def forbidden(request):
    return HttpResponseForbidden(render_to_string('403.html',
                                                  request=request))