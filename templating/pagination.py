from django.utils.http import urlencode
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from jinja2 import Markup
import urlparse

from redirect_middleware import HttpRedirect


def get_page_url(page_number, var_name, getvars=None, path=None):
    getvars = getvars or {}
    if var_name in getvars:
        del getvars['page']
    getvars[var_name] = page_number
    query = urlencode(getvars, doseq=True)
    return urlparse.urlunparse(('', '', path, '', query, ''))


class PaginationHelper(object):

    def __init__(self, queryset, request, var_name, paginate_by, results_limit):
        self.var_name = var_name
        self.request = request
        self.page_number = self.request.GET.get(self.var_name, 1)
        self.total = queryset.count()
        self.queryset = queryset[:results_limit]
        self.paginator = Paginator(self.queryset, paginate_by)

    def __nonzero__(self):
        # Should be exists() but there is bug in Django
        # http://code.djangoproject.com/ticket/18414
        return self.paginate().count()

    def get_page_url(self, page_number):
        getvars = dict(self.request.GET.iterlists())
        return get_page_url(page_number, self.var_name, getvars,
                            path=self.request.path)

    @property
    def num_pages(self):
        return self.paginator.num_pages

    def paginate(self):
        if not hasattr(self, '_page'):
            try:
                self._page = self.paginator.page(self.page_number)
            except (InvalidPage, EmptyPage):
                redirect_to = get_page_url(self.paginator.num_pages,
                                           self.var_name,
                                           self.request.GET.copy(), '')
                raise HttpRedirect(redirect_to)
        return self._page.object_list

    def pagination(self):
        from . import render_to_string
        return Markup(render_to_string('pagination.html', request=self.request, context={
            'total': self.total,
            'get_page_url': self.get_page_url,
            'page': self._page,
            'paginator': self.paginator,
        }))


def pagination(queryset, request, var_name='page', paginate_by=20, results_limit=None):
    return PaginationHelper(queryset, request, var_name, paginate_by, results_limit)

