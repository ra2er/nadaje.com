from functools import cmp_to_key
import urlparse
from django.utils.http import urlencode
from jinja2 import Markup, escape


class SortingHelper(object):

    sort_directions = {
        'asc': {'icon':'<i class="icon-chevron-up"></i>',
                'arrow': '&uarr;',
                'inverse': 'desc'},
        'desc': {'icon': '<i class="icon-chevron-down"></i>',
                 'arrow': '&darr;',
                 'inverse': 'asc'},
        '': {'icon': '<i class="icon-chevron-down"></i>',
             'arrow': '&darr;',
             'inverse': 'asc'},
    }

    def __init__(self, items, request, accepted_fields, sort_var_name,
                 direction_var_name, default_ordering):
        self.items = items
        self.accepted_fields = accepted_fields
        self.default_ordering = [default_ordering] if isinstance(default_ordering, basestring) else default_ordering
        self.request = request
        self.sort_var_name = sort_var_name
        self.direction_var_name = direction_var_name

    def sort(self):
        raise NotImplementedError()

    def _inverse_ordering(self, ordering):
        return [f[1:] if f.startswith('-') else '-%s'%f for f in ordering]

    def _get_current_sorting(self):
        getvars = dict(self.request.GET.iterlists())
        if self.sort_var_name in getvars:
            sorting = self.request.REQUEST.getlist(self.sort_var_name)
            result = []
            for field in sorting:
                if field in self.accepted_fields:
                    result.append(field)
            sorting = result
        elif self.default_ordering:
            sorting = self.default_ordering
        else:
            return [], None

        if self.direction_var_name in getvars:
            direction = getvars.get(self.direction_var_name, [''])[0]
        else:
            if all(f in self.accepted_fields for f in sorting):
                direction = 'desc'
            elif all(f in self.accepted_fields for f in self._inverse_ordering(sorting)):
                sorting = self._inverse_ordering(sorting)
                direction = 'asc'
            else:
                sorting = []
                direction = ''
        return sorting, direction

    def anchor(self, fields, title):
        if isinstance(fields, basestring):
            fields = [fields]
        current_sorting, direction = self._get_current_sorting()
        getvars = dict(self.request.GET.iterlists())

        anchor_text = title
        if fields == current_sorting:
            anchor_text = '%s %s' % (title, self.sort_directions[direction]['icon'])
            title = '%s %s' % (title, self.sort_directions[direction]['arrow'])
            getvars[self.direction_var_name] = self.sort_directions[direction]['inverse']
        else:
            getvars[self.direction_var_name] = 'desc'

        getvars[self.sort_var_name] = fields
        url = urlparse.urlunparse(('', '', self.request.path, None, urlencode(getvars, doseq=True), ''))
        return Markup('<a href="%s" title="%s">%s</a>' % (escape(url), title, anchor_text))

    def _get_ordering(self):
        if not hasattr(self, '_fields'):
            current_sorting, direction = self._get_current_sorting()
            if direction == 'asc':
                self._fields = self._inverse_ordering(current_sorting)
            else:
                self._fields = current_sorting
        return self._fields


class ListSortingHelper(SortingHelper):

    def __init__(self, *args, **kwargs):
        self.none_is_largest = kwargs.pop('none_is_largest', True)
        super(ListSortingHelper, self).__init__(*args, **kwargs)

    def sort(self):
        ordering = self._get_ordering()
        def _cmp(x, y):
            xt, yt = [], []
            for name in ordering:
                if name.startswith('-'):
                    xv = getattr(y, name[1:])
                    yv = getattr(x, name[1:])
                else:
                    xv = getattr(x, name)
                    yv = getattr(y, name)
                if xv is None or yv is None:
                    xt.append(1 if (xv is None) == self.none_is_largest else 0)
                    yt.append(1 if (yv is None) == self.none_is_largest else 0)
                else:
                    xt.append(xv)
                    yt.append(yv)
            return cmp(xt, yt)
        return sorted(self.items, key=cmp_to_key(_cmp))


def get_list_sorting(items, request, accepted_fields, default_ordering=None,
                     direction_var_name='direction', sort_var_name='sort',
                     none_is_largest=True):
    return ListSortingHelper(items, request, accepted_fields,
                             sort_var_name, direction_var_name,
                             default_ordering=default_ordering,
                             none_is_largest=none_is_largest)


class QuerysetSortingHelper(SortingHelper):

    def __init__(self, *args, **kwargs):
        self.background_orderings = kwargs.pop('background_orderings', None)
        self.replace = kwargs.pop('replace', True)
        super(QuerysetSortingHelper, self).__init__(*args, **kwargs)

    def sort(self):
        if self.replace:
            order_by = self._get_ordering()
        else:
            order_by = self.items.query.order_by + self._get_ordering()
        if self.background_orderings:
            order_by += self.background_orderings
        return self.items.order_by(*order_by)


def get_sorting(queryset, request, accepted_fields, default_ordering=None,
                direction_var_name='direction', sort_var_name='sort',
                background_orderings=None, replace=True):
    background_orderings = background_orderings or ['id']
    return QuerysetSortingHelper(queryset, request, accepted_fields,
                                 sort_var_name, direction_var_name,
                                 default_ordering=default_ordering,
                                 background_orderings=background_orderings,
                                 replace=replace)


def with_lower(queryset, source_attr, dest_attr):
    return queryset.extra(select={dest_attr: 'lower(%s.%s)' % (queryset.model._meta.db_table, source_attr)})


