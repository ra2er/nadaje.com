from django.http import HttpResponseRedirect

class HttpRedirect(Exception):
    def __init__(self, redirect_to):
        self.redirect_to = redirect_to


class RedirectExceptionMiddleware(object):
    def process_exception(self, request, exception):
        if isinstance(exception, HttpRedirect):
            return HttpResponseRedirect(exception.redirect_to)
