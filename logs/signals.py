from django.dispatch import Signal

station_add = Signal(providing_args=['station', 'author', 'desc1' 'desc2'])
station_edit = Signal(providing_args=['station', 'author', 'desc1' 'desc2'])
station_delete = Signal(providing_args=['station', 'author'])