from django.db import models
from django.utils.translation import ugettext_lazy as _
from logs import listeners
from stations.models import Station
from users.models import User
from utils import utcnow
from utils.db.models import Subtyped


class Action(Subtyped):

    user = models.ForeignKey(User)
    typ = models.CharField(_('type'), max_length=20,
                           choices=[('create', _('create')), ('read', _('read')),
                                    ('edit', _('edit')), ('delete', _('delete'))])
    created = models.DateTimeField(default=utcnow)


class StationAction(Action):

    station = models.ForeignKey(Station, null=True, blank=True, on_delete=models.SET_NULL,
                                related_name='actions')
    diff = models.TextField(_('diff'), blank=True)
    deleted_station_name = models.CharField(_('name'), blank=True, max_length=200)


listeners.start_listening()