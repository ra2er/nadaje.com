import difflib
import json
from logs import signals
from stations.models import Station


def station_diff(desc1, desc2):
    desc1 = desc1.splitlines()
    desc2 = desc2.splitlines()
    result = list(difflib.ndiff(desc1, desc2))
    return result


def add_station_create_action(sender, station, author, desc1, desc2, **kwargs):
    result = station_diff(desc1, desc2)
    station.actions.create(station=station, user=author, diff=json.dumps(result), typ='create')


def add_station_edit_action(sender, station, author, desc1, desc2, **kwargs):
    result = station_diff(desc1, desc2)
    station.actions.create(station=station, user=author, diff=json.dumps(result), typ='edit')


def add_station_delete_action(sender, station, author, **kwargs):
    station.actions.create(station=None, user=author, typ='delete',
                           deleted_station_name=station.name)


def start_listening():
    signals.station_add.connect(add_station_create_action, sender=Station)
    signals.station_edit.connect(add_station_edit_action, sender=Station)
    signals.station_delete.connect(add_station_delete_action, sender=Station)