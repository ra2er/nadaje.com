#-*- coding: utf-8 -*-
from __future__ import absolute_import
from . import models


class Broadcast(object):

    def __init__(self, city, transmitter, broadcaster):
        self.city = city
        self.transmitter = transmitter
        self.broadcaster = broadcaster
        super(Broadcast, self).__init__()

    @classmethod
    def from_city_and_transmitter(cls, city, transmitter):
        try:
            broadcaster = models.CityTransmitter.objects.get(city=city, transmitter=transmitter)
        except models.CityTransmitter.DoesNotExist:
            broadcaster = None
        return cls(city, transmitter, broadcaster)

    @classmethod
    def from_broadcaster(cls, broadcaster):
        return cls(broadcaster.city, broadcaster.transmitter, broadcaster)
