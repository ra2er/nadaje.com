#-*- coding: utf-8 -*-
from __future__ import absolute_import
from django.conf.urls import patterns, url

from . import views

urlpatterns = patterns('',
    url(r'^radio-transmitters/(?P<typ>(FM|DAB|INET))/$',
        views.radio_transmitter_index, name='radio-transmitter-index'),
    url(r'^radio-transmitters/location/(?P<location_pk>\d+)/(?P<typ>(FM|DAB|INET))/$',
        views.radio_transmitter_index, name='radio-transmitter-index'),
    url(r'^transmitter/location/(?P<pk>\d+)/(?P<typ>(FM|DAB|INET))/$',
        views.transmitter_location_details, name='transmitter-location-details'),
    url(r'^transmitter/location/(?P<pk>\d+)/$',
        views.transmitter_location_details, name='transmitter-location-details'),
    url(r'^(?P<typ>(FM|DAB|INET))/cities/$',
        views.city_index, name='city-index'),
    url(r'^cities/$', views.city_index, name='city-index'),
    url(r'^cities/city/(?P<pk>\d+)/$', views.city_details, name='city-details'),
    url(r'^(?P<typ>(FM|DAB|INET))/cities/city/(?P<pk>\d+)/$',
        views.city_details, name='city-details'),
    url(r'^cities/city/(?P<pk>\d+)/(?P<slug>(.*))/$', views.city_details, name='city-details'),
    url(r'^(?P<typ>(FM|DAB|INET))/cities/city/(?P<pk>\d+)/(?P<slug>(.*))/$',
        views.city_details, name='city-details'),
    url(r'^foreign/$', views.foreign_transmitter_index, name='foreign-index'),
    url(r'^polish-radio-map/$', views.map, name='map'),
    url(r'^polish-radio-map/city/(?P<city_pk>\d+)/$',
        views.map, name='map-city'),
    url(r'^polish-radio-map/location/(?P<location_pk>\d+)/$',
        views.map, name='map-location'),
)