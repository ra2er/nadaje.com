from __future__ import absolute_import
from . import Broadcast
from markupsafe import Markup
from templating import register_global, register_filter
from transmitters.models import REGIONS


def broadcaster(city, transmitter):
    broadcast = Broadcast.from_city_and_transmitter(city, transmitter)
    return broadcast.broadcaster


def broadcaster_quality_format(broadcaster):
    quality = int(broadcaster.quality)
    star_full = '<span class="star">&nbsp;<i class="icon-star"></i></span>'
    star_empty = '<span class="star">&nbsp;<i class="icon-star-empty"></i></span>'
    rating = ''
    for i in xrange(quality):
        rating += star_full
    for i in xrange(5-quality):
        rating += star_empty
    tooltip = 'Broadcasting quality: %(quality)s' % {'quality': broadcaster.get_quality_display()}
    return Markup('<div class="broadcast-quality" '
                  'data-toggle="tooltip" '
                  'data-original-title="%(tooltip)s">%(rating)s</div>' % {'rating': rating, 'tooltip': tooltip})


def code_to_region(code):
    return REGIONS.get(code, [None])[0]


def register_globals(env):
    register_global(broadcaster, 'broadcaster')

def register_filters(env):
    register_filter(broadcaster_quality_format, 'broadcast_quality_format')
    register_filter(code_to_region, 'code_to_region')
