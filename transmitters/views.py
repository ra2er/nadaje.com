#-*- coding: utf-8 -*-
from __future__ import absolute_import
from django.core.urlresolvers import reverse
from django.http import HttpResponse, Http404
from django.shortcuts import get_object_or_404
from django.utils import simplejson

from . import models, forms
from templating import render_to_response
from transmitters.models import REGIONS


def radio_transmitter_index(request, typ):
    if typ == 'FM':
        transmitters = models.FMTransmitter.objects.all()
    elif typ == 'DAB':
        transmitters = models.DABTransmitter.objects.all()
    else:
        raise Http404
    filter_form = forms.TransmitterFilterForm(data=request.GET or None,
                                              transmitters=transmitters)
    if filter_form.is_valid():
        region = filter_form.cleaned_data['region']
        transmitters = filter_form.search()
    elif not filter_form.is_bound:
        region='DS'
        transmitters = transmitters.filter(location__region=region)
    else:
        region = None
        transmitters = None
    context = {
        'transmitters': transmitters,
        'typ': typ,
        'region': region,
        'filter_form': filter_form
    }
    return render_to_response('transmitters/radio_transmitter_index.html',
                              request=request, context=context)


def foreign_transmitter_index(request):
    transmitters = models.RadioTransmitter.objects.exclude(
        location__country='PL')
    filter_form = forms.ForeignTransmitterFilterForm(data=request.GET or None,
                                                     transmitters=transmitters)
    if filter_form.is_valid():
        country = filter_form.cleaned_data['country']
        transmitters = filter_form.search()
    elif not filter_form.is_bound:
        country = 'all'
        transmitters = transmitters.distinct()
    else:
        country = None
        transmitters = None
    context = {
        'transmitters': transmitters,
        'country': country,
        'filter_form': filter_form
    }
    return render_to_response('transmitters/foreign_transmitter_index.html',
                              request=request, context=context)


def tv_transmitter_index(request):
    transmitters = models.TVTransmitter.objects.all()
    context = {
        'transmitters': transmitters,
    }
    return render_to_response('transmitters/radio_transmitter_index.html',
                              request=request, context=context)


def transmitter_location_details(request, pk, typ='FM'):
    transmitter_location = get_object_or_404(models.TransmitterLocation, pk=pk)
    transmitters = [t.get_subtype_instance() for t in
                    transmitter_location.transmitters.filter(active=True)]
    context = {
        'transmitter_location': transmitter_location,
        'transmitters': transmitters,
        'typ': typ
    }
    if request.is_ajax():
        data = {
            'transmitter_location': transmitter_location.location,
            'transmitter_location_url':
            (reverse('admin:transmitter-location-details',
                     args=[transmitter_location.pk])
                if request.user and request.user.is_admin
                else reverse('transmitters:transmitter-location-details',
                             args=[transmitter_location.pk])),
            'lat': transmitter_location.lat,
            'lng': transmitter_location.lng,
            'transmitters': [
                {
                    'station': transmitter.station.name,
                    'station_url':
                        (reverse('admin:station-details',
                                 args=[transmitter.station.pk])
                            if request.user and request.user.is_admin
                            else reverse('radio-stations:station-details',
                                         args=[transmitter.station.pk])),
                    #FIXME: there is currently no tv transmitters in db
                    'frequency_or_channel':
                    getattr(transmitter,
                            'channel', '%s MHz' % getattr(transmitter,
                                                          'frequency', ''))
                } for transmitter in transmitters
            ]
        }
        return HttpResponse(simplejson.dumps(data))
    return render_to_response('transmitters/location_details.html',
                              request=request, context=context)


def city_index(request, typ=None):
    region = 'DS'
    if typ == 'FM':
        cities = models.City.objects.filter(
            transmitters__in=models.FMTransmitter.objects.all()).distinct()
    elif typ == 'DAB':
        cities = models.City.objects.filter(
            transmitters__in=models.DABTransmitter.objects.all()).distinct()
    else:
        cities = models.City.objects.all()
    filter_form = forms.CityFilterForm(cities=cities, data=request.GET or None)
    if filter_form.is_valid():
        cities = filter_form.search()
        region = filter_form.cleaned_data['region']
    elif not filter_form.is_bound:
        cities = cities.filter(region=region).order_by('name')
    context = {
        'cities': cities,
        'typ': typ,
        'region': region,
        'filter_form': filter_form
    }
    return render_to_response('transmitters/city_index.html',
                              request=request, context=context)


def city_details(request, pk, typ='FM', slug=None):
    show = request.GET.get('show', None)
    city = get_object_or_404(models.City, pk=pk)
    transmitters = models.FMTransmitter.objects.filter(cities=city).distinct()
    if show is not None:
        if show == 'all':
            transmitters = transmitters
        if show == 'foreign':
            transmitters = transmitters.exclude(location__country='PL').distinct()
    else:
        transmitters = models.FMTransmitter.objects.filter(citytransmitter__city=city, citytransmitter__quality__gte=3).distinct()
    context = {
        'city': city,
        'transmitters': transmitters,
        'typ': typ,
        'show': show
    }
    if request.is_ajax():
        data = {
            'city': city.name,
            'city_url':
            (reverse('admin:transmitter-city-details',
                     args=[city.pk])
                if request.user and request.user.is_admin
                else reverse('transmitters:city-details',
                             args=[city.pk])),
            'lat': city.lat,
            'lng': city.lng,
            'transmitters': [
                {
                    'station': transmitter.station.name,
                    'station_url':
                        (reverse('admin:station-details',
                                 args=[transmitter.station.pk])
                            if request.user and request.user.is_admin
                            else reverse('radio-stations:station-details',
                                         args=[transmitter.station.pk])),
                    #FIXME: there is currently no tv transmitters in db
                    'frequency_or_channel':
                    getattr(transmitter,
                            'channel', '%s MHz' % getattr(transmitter,
                                                          'frequency', ''))
                } for transmitter in transmitters
            ]
        }
        return HttpResponse(simplejson.dumps(data))
    return render_to_response('transmitters/city_details.html',
                              request=request, context=context)


def map(request, city_pk=None, location_pk=None, country='PL', typ='FM'):
    region = 'DS'
    cities = locations = []
    form = forms.MapForm(data=request.GET or None)
    if any([city_pk is not None, location_pk is not None]):
        if city_pk:
            city = get_object_or_404(models.City, pk=city_pk)
            center = {'lat': city.lat, 'lng': city.lng, 'zoom': 9}
            cities = [city]
        elif location_pk:
            location = get_object_or_404(models.TransmitterLocation,
                                         pk=location_pk)
            center = {'lat': location.lat, 'lng': location.lng, 'zoom': 9}
            locations = [location]
        else:
            raise Http404
    else:
        if form.is_valid():
            region = form.cleaned_data['region']
            show_what = form.cleaned_data['show_what']
            if show_what == 'cities':
                cities = models.City.objects.filter(country=country,
                                                    region=region)
            elif show_what == 'transmitters':
                locations = models.TransmitterLocation.objects.filter(
                    country=country, region=region)
        elif not form.is_bound:
            cities = models.City.objects.filter(country=country, region=region)
        center = {'lat': REGIONS[region][1], 'lng': REGIONS[region][2],
                  'zoom': REGIONS[region][3]}
    context = {
        'locations': locations,
        'cities': cities,
        'region': region,
        'form': form,
        'typ': typ,
    }
    context.update(center=center)
    return render_to_response('transmitters/map.html',
                              request=request, context=context)
