#-*- coding: utf-8 -*-
from __future__ import absolute_import
from django.db import models
from django.utils.translation import ugettext_lazy as _
from stations.models import RadioStation, TVStation

from utils import countries
from utils.db.models import Subtyped


REGIONS = {
    'DS': (u'dolnośląskie', 50.903033, 16.424561, 8),
    'KP': (u'kujawsko-pomorskie', 53.16324, 18.484497, 8),
    'LU': (u'lubelskie', 51.248163, 23.098755, 8),
    'LB': (u'lubuskie', 52.227799, 15.26001, 8),
    'LD': (u'łódzkie', 51.488224, 19.346924, 8),
    'MP': (u'małopolskie', 49.827353, 20.083008, 8),
    'MA': (u'mazowieckie', 52.318553, 20.967407, 8),
    'OP': (u'opolskie', 50.632042, 17.896729, 9),
    'PK': (u'podkarpackie', 50.057139, 22.088013, 8),
    'PD': (u'podlaskie', 53.284922, 22.653809, 8),
    'PM': (u'pomorskie', 54.367759, 18.045044, 8),
    'SL': (u'śląskie', 50.17338, 19.02832, 8),
    'SW': (u'świętokrzyskie', 50.625073, 20.939941, 8),
    'WM': (u'warmińsko-mazurskie', 53.748711, 20.928955, 8),
    'WP': (u'wielkopolskie', 52.281602, 17.347412, 8),
    'ZP': (u'zachodniopomorskie', 53.553363, 15.540161, 8),
}


REGION_CHOICES = ((k, v[0]) for k, v in REGIONS.items())


class TransmitterLocation(models.Model):

    location = models.CharField(_('location'), max_length=200)
    country = models.CharField(_('country'), max_length=2, choices=countries.COUNTRY_CHOICES)
    region = models.CharField(_('region'), max_length=2, choices=REGION_CHOICES, blank=True)
    lat = models.CharField(_('latitude'), max_length=15, blank=True)
    lng = models.CharField(_('longitude'), max_length=15, blank=True)

    def __unicode__(self):
        return self.location


class Transmitter(Subtyped):

    location = models.ForeignKey(TransmitterLocation, related_name='transmitters')
    altitude = models.IntegerField(_('altitude'), null=True, blank=True)
    active = models.BooleanField(_('active'), default=False)

    @property
    def country(self):
        return self.location.get_country_display()

    @property
    def region(self):
        return self.location.get_region_display() if self.location.region else ''

    def __unicode__(self):
        raise NotImplementedError()


class RadioTransmitter(Transmitter):

    station = models.ForeignKey(RadioStation, related_name='transmitters')


class FMTransmitter(RadioTransmitter):

    frequency = models.FloatField(_('frequency'), null=True, blank=True)
    erp = models.CharField(_('erp'), max_length=10, blank=True)
    polarity = models.CharField(_('polarity'), max_length=1, blank=True,
                                choices=[('H', _('horizontal')), ('V', _('vertical'))])
    rds = models.BooleanField(_('RDS signal'), default=True)
    pi = models.CharField(_('PI'), max_length=10, blank=True)

    class Meta:
        ordering = ('frequency', )

    @property
    def broadcast_type(self):
        return 'FM'

    def __unicode__(self):
        return u'%s %s MHz' %(self.station.name, self.frequency)


class DABTransmitter(RadioTransmitter):

    multiplex = models.CharField(_('multiplex'), max_length=10)
    channel = models.CharField(_('channel'), max_length=10)
    codec = models.CharField(_('codec'), max_length=10, blank=True)

    def __unicode__(self):
        return u'%s MUX: %s, CH: %s' %(self.station.name, self.multiplex, self.channel)

    @property
    def broadcast_type(self):
        return 'DAB'


class TVTransmitter(Transmitter):

    station = models.ForeignKey(TVStation, related_name='transmitters')

    def __unicode__(self):
        return self.station


#TODO: Create digital type tv transmitter


class City(models.Model):

    name = models.CharField(_('name'), max_length=50)
    slug = models.SlugField(max_length=50, unique=True, editable=False, null=True)
    country = models.CharField(_('country'), max_length=2, blank=True,
                               choices=countries.COUNTRY_CHOICES, default='PL')
    region = models.CharField(_('region'), max_length=2, choices=REGION_CHOICES, blank=True)
    lat = models.CharField(_('latitude'), max_length=15, blank=True)
    lng = models.CharField(_('longitude'), max_length=15, blank=True)

    transmitters = models.ManyToManyField(Transmitter, related_name='cities',
                                          through='CityTransmitter')

    def __unicode__(self):
        return self.name


class CityTransmitter(models.Model):

    city = models.ForeignKey(City)
    transmitter = models.ForeignKey(Transmitter)
    quality = models.IntegerField(_('quality'), default=1,
                                  choices=[(1, _('very low')), (2, _('low')),
                                           (3, _('decent')), (4, _('good')),
                                           (5, _('very good'))])

    def __unicode__(self):
        return u'%s: %s' % (self.city, self.transmitter.get_subtype_instance())
