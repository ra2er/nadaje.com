from __future__ import absolute_import
from django import forms
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _
from transmitters.models import REGIONS
from utils.countries import COUNTRY_CHOICES
from utils.forms import SelfAwareFormMixin, QueryField


class TransmitterFilterForm(SelfAwareFormMixin):

    query = QueryField(label='&nbsp;', required=False, help_text='',
                       widget=forms.TextInput(attrs={'placeholder':
                                                     _('city, location')}))
    region = forms.ChoiceField(
        label='&nbsp;',
        choices=sorted([(k, v[0]) for k, v in REGIONS.items()],
                       key=lambda choice: choice[1]),
        initial='DS')

    def __init__(self, transmitters, *args, **kwargs):
        self.transmitters = transmitters
        super(TransmitterFilterForm, self).__init__(*args, **kwargs)

    def search(self):
        transmitters = self.transmitters
        parts = self.cleaned_data.get('query')
        if parts:
            query = Q()
            for part in parts:
                query &= (Q(location__location__icontains=part) |
                          Q(station__name__icontains=part))
            transmitters = transmitters.filter(query)
        region = self.cleaned_data.get('region')
        if region:
            transmitters = transmitters.filter(location__region=region)
        return transmitters.distinct()


class ForeignTransmitterFilterForm(SelfAwareFormMixin):

    query = QueryField(label='&nbsp;', required=False, help_text='',
                       widget=forms.TextInput(attrs={'placeholder':
                                                     _('city, location')}))
    country = forms.ChoiceField(label='&nbsp;', required=False, help_text='',
                                initial='all')

    def __init__(self, transmitters, *args, **kwargs):
        self.transmitters = transmitters
        super(ForeignTransmitterFilterForm, self).__init__(*args, **kwargs)
        countries_list = [v[0] for v in
                          self.transmitters.values_list('location__country').distinct()]
        countries = dict(COUNTRY_CHOICES)
        country_choices = [('all', _('All'))]
        for country_code in countries_list:
            if not country_code:
                continue
            country_choices.append((country_code, countries[country_code]))
        country_choices = sorted(country_choices,
                                 key=lambda country_choice: country_choice[1])
        self.fields['country'].choices = country_choices

    def search(self):
        transmitters = self.transmitters
        parts = self.cleaned_data.get('query')
        if parts:
            query = Q()
            for part in parts:
                query &= (Q(location__location__icontains=part) |
                          Q(fmtransmitter__station__name__icontains=part) |
                          Q(dabtransmitter__station__name__icontains=part))
            transmitters = transmitters.filter(query)
        country = self.cleaned_data.get('country')
        if country and not country == 'all':
            transmitters = transmitters.filter(station__country=country)
        return transmitters.distinct()


class CityFilterForm(SelfAwareFormMixin):

    query = QueryField(label='&nbsp;', required=False, help_text='',
                       widget=forms.TextInput(attrs={'placeholder':
                                                     _('name')}))
    region = forms.ChoiceField(
        label='&nbsp;',
        choices=sorted([(k, v[0]) for k, v in REGIONS.items()],
                       key=lambda choice: choice[1]),
        initial='DS')

    def __init__(self, cities, *args, **kwargs):
        self.cities = cities
        super(CityFilterForm, self).__init__(*args, **kwargs)

    def search(self):
        cities = self.cities
        parts = self.cleaned_data.get('query')
        if parts:
            query = Q()
            for part in parts:
                query &= (Q(name__icontains=part))
            cities = cities.filter(query)
        region = self.cleaned_data.get('region')
        if region:
            cities = cities.filter(region=region)
        return cities.distinct().order_by('name')


class MapForm(SelfAwareFormMixin):

    region = forms.ChoiceField(label='&nbsp;',
                               choices=[(k, v[0]) for k, v in REGIONS.items()],
                               initial='DS')
    show_what = forms.ChoiceField(label='&nbsp;',
                                  choices=[('cities', _('Cities')),
                                           ('transmitters', _('Transmitters'))],
                                  initial='cities')

