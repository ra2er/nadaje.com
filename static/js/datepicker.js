define(['jquery', 'lib/bootstrap-datepicker'], function($) {
    $.fn.datepicker.dates.pl = {
            days: ["Niedziela", "Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota", "Niedziela"],
            daysShort: ["Nie", "Pn", "Wt", "Śr", "Czw", "Pt", "So", "Nie"],
            daysMin: ["N", "Pn", "Wt", "Śr", "Cz", "Pt", "So", "N"],
            format: 'dd.mm.yyyy',
            months: ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"],
            monthsShort: ["Sty", "Lu", "Mar", "Kw", "Maj", "Cze", "Lip", "Sie", "Wrz", "Pa", "Lis", "Gru"],
            today: "Dzisiaj",
            weekStart: 1
    };
    $.fn.datepicker.dates.en.format = 'yyyy-mm-dd';
    // a bit hacky way of configuration - look into base.html there is LANGUAGE_CODE defined
    $.fn.datepicker.defaults.language = LANGUAGE_CODE;
});
