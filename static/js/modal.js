define(['jquery', 'settings', 'utils', 'lib/jquery.ui.widget',
        'lib/bootstrap', 'ajaxForm', 'lib/jquery.blockUI'], function($, settings, utils) {
    $.widget('nadaje.modalBase', {
        options: {
            // params: @response, @dialog
            success: function(response, dialog) {},
            width: settings.MODAL_NARROW
        },
        modalTemplate: $([
            '<div class="modal in">',
              '<div class="modal-header">',
                '<a class="close" data-dismiss="modal">×</a>',
                '<h3>', '<img src="', settings.THROBBER, '" />', '</h3>',
              '</div>',
              '<div class="modal-body">',
                '<p style="text-align:center">',
                    '<img src="', settings.THROBBER, '" />',
                '</p>',
              '</div>',
            '</div>'
        ].join('')),
        _fetchTitle: function(response) {
            var title = utils.filterHtml(response, '.modal-header');
            if(title.length === 0) {
                title = utils.filterHtml(response, '#submenu .active');
            }
            return title;
        },
        _onSuccess: function(response, dialog) {
            this.options.success(response, dialog);
        },
        _openDialog: function() {
            var dialog = this.modalTemplate.clone();
            var zIndex;
            $('.modal.in').each(function() {
                var z = parseInt($(this).css('z-index'), 10);
                if(zIndex === undefined || z > zIndex) {
                    zIndex = z;
                }
            });
            dialog.css({
                'margin-left': -this.options.width/2 + 'px',
                'width': this.options.width + 'px'
            });
            $(dialog).modal({
                'hidden': function() {
                    dialog.remove();
                }
            });
            if(zIndex !== undefined) {
                var modal = dialog.data('modal');
                modal.$backdrop.css({
                    'z-index': zIndex + 1
                });
                modal.$element.css({
                    'z-index': zIndex + 2
                });
            }
            return dialog;
        },
        _fitModalHeight: function(dialog) {
            var viewPortHeight, modalHeight, spareSpace;
            var modalBody = $('.modal-body', dialog);
            var invisibleHeight = modalBody[0].scrollHeight - parseInt(modalBody.css('max-height'), 10);

            if(invisibleHeight > 0) {
                viewPortHeight = $(window).height();
                modalHeight = $(dialog).height();
                spareSpace = viewPortHeight - modalHeight;

                spareSpace = Math.min(spareSpace, invisibleHeight);
                modalBody.css('max-height', modalBody.height() + spareSpace);
                spareSpace = viewPortHeight - $(dialog).height();
                dialog.css('top', spareSpace/2 + 'px');
                dialog.css('margin-top', '0');
            }
        },
        _create: function() {
            var that = this;
            that._bind(that.element, {
                click: function(event) {
                    var url = $(that.element).attr('href');
                    var dialog = that._openDialog();
                    $.ajax({
                        url: url,
                        success: function(response, textStatus, xhr) {
                            that._onSuccess(response, dialog, xhr);
                        },
                        error: function(xhr) {
                            window.location.reload();
                        }
                    });
                    return false;
                }
            });
        }
    });

    var ModalForm = function(html, dialog) {
        if(this === window) {
            return new ModalForm(element, options);
        }
        this.init(element, options);
    };

    $.widget('nadaje.modalEdit', $.nadaje.modalBase, {
        options: {
            initForm: function(form, dialog, step) {},
            // on form submit success
            onSubmitSuccess: function(response, dialog, widget, xhr) {
                document.location = xhr.responseText;
            },
            onSubmitComplete: function(xhr, textStatus, dialog, that) {},
            stepsCount: 1
        },
        _create: function() {
            var that = this;
            var domElement = this.element.get(0);
            var eventType, data, method;
            if(domElement.tagName.toLowerCase() === 'form') {
                urlAttr = 'action';
                eventType = 'submit';
                method = that.element.attr('method') || 'get';
            } else {
                urlAttr = 'href';
                eventType = 'click';
                method = 'get';
            }
            var handlers = {};
            handlers[eventType] = function(event) {
                var dialog = that._openDialog();
                that.url = that.element.attr(urlAttr);
                that.step = 1;
                if(domElement.tagName.toLowerCase() === 'form') {
                    data = that.element.serialize();
                }
                that._fetchFormPage(dialog, method, data);
                return false;
            };
            that._bind(that.element, handlers);
        },
        _fetchFormPage: function(dialog, method, data) {
            var that = this;
            $.ajax({
                data: data,
                error: function(xhr) {
                    window.location.reload();
                },
                success: function(response, textStatus, xhr) {
                    if(xhr.status === 201) {
                        that.step += 1;
                        that.url = response;
                        that._fetchFormPage(dialog, 'get');
                    } else {
                        that._onSuccess(response, dialog, xhr);
                    }
                },
                type: method,
                url: that.url
            });
        },
        _formToModalParts: function(form, dialog) {
            var that = this;
            // build bootstrap modal body from given form
            var actions = $('.form-actions', form);
            var modalBody = $('<div class="modal-body"></div>');

            actions.removeClass('form-actions').addClass('modal-footer');
            modalBody.append(form.children().not(actions));
            form.prepend(modalBody);
            form.css('margin', 0);

            that._bind($('.cancel', form), {
                click: function(event) {
                    dialog.modal('hide');
                    return false;
                }
            });
            that._fitModalHeight(dialog);
        },
        _onSuccess: function(response, dialog) {
            var that = this;
            var form = utils.filterHtml(response, 'form');
            var title = that._fetchTitle(response);
            $('.modal-body', dialog).remove();
            $('.modal-header h3', dialog).text(title.text());

            dialog.append(form);

            form.initAjaxForm({
                beforeSubmit: function(form) {
                    utils.blockElement(dialog, {
                        baseZ: dialog.css('z-index')
                    });
                },
                complete: function(xhr, textStatus) {
                    if(xhr.status !== 201 || xhr.status !== 200) {
                        dialog.unblock();
                    }
                    that.options.onSubmitComplete(xhr, textStatus, dialog, that);
                    return true;
                },
                initForm: function(form) {
                    that._formToModalParts(form, dialog);
                    if(form.attr('action')) {
                        that.url = form.attr('action');
                    } else {
                        form.attr('action', that.url);
                    }
                    that.options.initForm(form, dialog, that.step);
                },
                success: function(response, form, xhr) {
                    if(that.step >= that.options.stepsCount) {
                        that.options.onSubmitSuccess(response, dialog, that, xhr);
                    } else {
                        dialog.unblock();
                    }
                    that.step += 1;
                    that.url = response;
                    $('.modal-footer', dialog).remove();
                    that._fetchFormPage(dialog, 'get');
                }
            });
            that.options.success(response, dialog);
        }
    });

    $.widget('nadaje.modalDetails', $.nadaje.modalBase, {
        _onSuccess: function(response, dialog, selector) {
            var that = this;
            selector = selector || '#content';
            var content = utils.filterHtml(response, selector);
            var title = this._fetchTitle(response);
            $('.modal-body', dialog).remove();
            $('.modal-header h3', dialog).text(title.text());
            dialog.append($('<div class="modal-body"></div>').append(content));
            that._fitModalHeight(dialog);
            that.options.success(response, dialog, selector);
        }
    });
});
