define(['jquery', 'settings', 'utils', 'lib/jquery.ui.widget'], function($, settings, utils) {
    $.widget('nadaje.searchBox', {
        _create: function() {
            var that = this;
            that.url = that.element.attr('action');
            that.method = that.element.attr('method');
            that.query = $(that.element).find('input[name=query]');
            that.results = $(that.element).find('#search-results');
            that._bindField(that.query);
            $(document).mouseup(function(e){
                if (!that.element.is(e.target)
                    && that.element.has(e.target).length === 0) {
                    that.results.hide();
                } else {
                    that.results.show();
                }
            });
        },
        _bindField: function(input) {
            var that = this;
            var eventType = input.attr('type')==='text'?'keyup':'change';
            var handlers = {};
            handlers[eventType] = function(e) {
                if (e.which !== 40 && e.which !== 38){
                    if(that.timer !== undefined) {
                        clearTimeout(that.timer);
                    }
                    that.timer =  setTimeout(function() {
                        that._submit();
                    }, 500);
                }
            };
            that._bind(input, handlers);
        },
        _submit: function() {
            var that = this;
            $.ajax({
                url: that.url,
                method: that.method,
                dataType: 'json',
                data: that.element.serialize(),
                success: function(response) {
                    that._showResult(response);
                }
            })
        },
        _showResult: function(response) {
            var that = this;
            template = that._getResultsTemplate(response);
            that.results.html(template);
        },
        _getResultsTemplate: function(items) {
            var that = this;
            var template = $('<ul class="dropdown"></ul>');
            for (var i=0; i<items.length; i++) {
                template.append($(['<li>', '<a href="', items[i]['url'], '">',
                                   '<img src="', items[i]['thumbnail_url'],
                                   '" alt="station-logo" class="img-rounded">', items[i]['name'],
                                   '</a>'].join('')))
            }
            that._bindTemplateNavigation(template);
            return template;
        },
        _bindTemplateNavigation: function(template) {
            var that = this;
            var li = $(template).find('li');
            var liSelected;
            that.query.keydown(function(e) {
                if (e.which === 40) {
                    if (liSelected){
                        liSelected.removeClass('selected');
                        next = liSelected.next();
                        if (next.length > 0){
                            liSelected = next.addClass('selected');
                        } else {
                            liSelected = li.eq(0).addClass('selected');
                        }
                    } else {
                        liSelected = li.eq(0).addClass('selected');
                    }
                } else if (e.which === 38){
                    if (liSelected) {
                        liSelected.removeClass('selected');
                        next = liSelected.prev();
                        if (next.length > 0) {
                            liSelected = next.addClass('selected');
                        } else {
                            liSelected = li.last().addClass('selected');
                        }
                    } else {
                        liSelected = li.last().addClass('selected');
                    }
                } else if (e.which === 13) {
                    if (liSelected) {
                        e.preventDefault();
                        window.location = liSelected.find('a').attr('href');
                    }

                }
            });
        }
    });
});
