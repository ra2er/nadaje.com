define(['jquery', 'settings', 'utils', 'goog!maps,3.exp,other_params:sensor=false',
        'lib/jquery.ui.widget'], function($, settings, utils) {
    $.widget('nadaje.googleMap', {
        options: {
            zoom: '12',
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            centerLat: '51.769801',
            centerLng: '16.1658847',
            width: 'inherit',
            height: '250',
            showInfoBox: true,
            infoBoxPixelOffset: '10'
        },
        _create: function() {
            var that = this;
            that.markers = [];
            var mapOptions = {
                zoom: that.options.zoom,
                center: new google.maps.LatLng(that.options.centerLat, that.options.centerLng),
                mapTypeId: that.options.mapTypeId
            };
            that.mapCanvas = $('#map-canvas', that.element);
            that.mapCanvas.width(that.options.width).height(that.options.height);
            that.map = new google.maps.Map(that.mapCanvas.get(0), mapOptions);
            var poiList = $(that.element).find('a.marker').hide();
            poiList.each(function() {
                that._initPoi($(this));
            })

        },
        _initPoi: function(poi) {
            var that = this;
            that.markers.push(that._setMarker(poi.attr('data-lat'),
                                              poi.attr('data-lng'),
                                              poi.attr('data-type'),
                                              poi.attr('data-object-url'),
                                              poi.attr('data-label')));
        },
        _setMarker: function(lat, lng, type, objectUrl, label, markerIcon) {
            var that = this;
            var marker =  new google.maps.Marker({
                map: that.map,
                icon: markerIcon,
                position: new google.maps.LatLng(lat, lng),
                title: label
            });
            if (that.options.showInfoBox) {
                google.maps.event.addListener(marker, 'click', function() {
                    $.ajax({
					    url: objectUrl,
                        type: 'GET',
                        dataType: 'json',
                        success: function(data){
                            var infoBox = new google.maps.InfoWindow({
                                content: that._getInfoBoxTemplate(type, label, data).html(),
                                pixelOffset: that.options.pixelOffset
                            });
                            infoBox.open(that.map, marker);
                        },
                        error: function(e){
							var infoBox = new google.maps.InfoWindow({
                                content: e
                            });
                            infoBox.open(that.map, marker);
                        }
                    });
                });
            }
            return marker;
        },
        _getInfoBoxTemplate: function(type, label, data){
            var template = $('<div></div>');
            var rows = $('<div></div>');
            if (type === 'transmitter') {
                for (i=0; i<data['transmitters'].length; i++) {
                    rows.append($([
                        '<tr>',
                            '<td class="first">',
                                data['transmitters'][i]['frequency_or_channel'],
                            '</td>',
                            '<td>',
                                '<a href="', data['transmitters'][i]['station_url'], '">', data['transmitters'][i]['station'], '</a>',
                            '</td>',
                        '</tr>'
                    ].join('')))
                }
                template.append($([
                    '<div id="markerContent-overlay">',
                        '<a class="heading" href="', data['transmitter_location_url'], '">', data['transmitter_location'], '</a>',
                        '<div id="markerContent-content">',
                            '<table class="table table-striped">',
                                '<tbody>',
                                    rows.html(),
                                '</tbody>',
                            '</table>',
                        '</div>',
                    '</div>'
                ].join('')));
            }
            if (type === 'city') {
                for (i=0; i<data['transmitters'].length; i++) {
                    rows.append($([
                        '<tr>',
                            '<td class="first">',
                                data['transmitters'][i]['frequency_or_channel'],
                            '</td>',
                            '<td>',
                                '<a href="', data['transmitters'][i]['station_url'], '">', data['transmitters'][i]['station'], '</a>',
                            '</td>',
                        '</tr>'
                    ].join('')))
                }
                template.append($([
                    '<div id="markerContent-overlay">',
                        '<a class="heading" href="', data['city_url'], '">', data['city'], '</a>',
                        '<div id="markerContent-content">',
                            '<table class="table table-striped">',
                                '<tbody>',
                                    rows.html(),
                                '</tbody>',
                            '</table>',
                        '</div>',
                    '</div>'
                ].join('')));
            }
            return template
        }
    });
});
