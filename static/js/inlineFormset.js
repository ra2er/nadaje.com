define(['jquery', 'lib/jquery.ui.widget'], function($) {
    $.widget('nadaje.inlineFormset', {
        options: {
            addButtonLabel: 'Add',
            initSubform: $.noop,
            required: false,
            initFormsNum: 0
        },
        _create: function() {
            var that = this;
            that.totalFormsInput = $('input[name$=TOTAL_FORMS]', that.element);
            that.formsetPrefix = $('input[name$=TOTAL_FORMS]', that.element).attr('name').split('-')[0];
            that.maxNumForms = parseInt($('input[name$=MAX_NUM_FORMS]', that.element).val(), 10);
            if (that.options.initFormsNum == that.maxNumForms) {
                that.extraSubForm = $('.subform:last', that.element);
                that.extraSubForm = that.extraSubForm.clone();
            } else {
                that.extraSubForm = $('.subform:last', that.element).hide();
            }
            $('.subform', that.element).not(that.extraSubForm).each(function() {
                that._initSubform($(this));
            });
            that.addButton = $('<input class="placeholder" type="text" disabled="disabled" /> <a href="#" class="add with-icon"><i class="icon-plus-sign"></i> ' +
                               that.options.addButtonLabel +
                               '</a>');
            $('.control-group:last', that.element).append($('<div class="controls"></div>').append(that.addButton));
            that.addButton.click(function() {
                that._createNewSubForm();
                return false;
            });
            if(that.options.required && parseInt(that.totalFormsInput.val(), 10) === 1) {
                that._createNewSubForm(true);
            }
            if (that.options.initFormsNum == that.maxNumForms) {
                that.addButton.hide();
            }
        },
        _initSubform: function(form) {
            var that = this;
            var deleteLabel = form.find('label.delete').hide();
            var deleteCheckbox = deleteLabel.find('input');
            var allForms = $('.subform', that.element).not(that.extraSubForm).filter(':not(:has(label.delete input:checked))');
            if(deleteCheckbox.is(':checked')) {
                form.hide();
            }
            var deleteButton = $('<a class="delete with-icon" href="#" title="delete">' +
                                 '<i class="icon-remove-sign"></i> ' + 'Remove' +
                                 '</a>');
            deleteLabel.after(deleteButton);
            if(that.options.required) {
                if(allForms.length <= 1) {
                    deleteButton.hide();
                } else {
                    allForms.find('a.delete').show();
                }
            }
            if (that.addButton != undefined) {
                if (that.maxNumForms <= allForms.length) {
                    that.addButton.hide();
                }
            }
            deleteButton.click(function() {
                form.fadeOut(function() {
                    if (that.addButton != undefined) {
                        if (that.maxNumForms >= allForms.length) {
                            that.addButton.show();
                        }
                    }
                });
                deleteCheckbox.attr('checked', true);
                if(that.options.required) {
                    allForms = $('.subform', that.element).not(that.extraSubForm).filter(':not(:has(label.delete input:checked))');
                    if(allForms.length <= 1) {
                        allForms.find('a.delete').fadeOut();
                    }
                }
                return false;
            });
            that.options.initSubform(form);
        },
        _createNewSubForm: function(quick) {
            quick = quick===undefined?false:quick;
            var that = this;
            var newSubForm = that.extraSubForm;
            var count = parseInt(that.totalFormsInput.val(), 10);

            that.extraSubForm = that.extraSubForm.clone();
            newSubForm.after(that.extraSubForm);

            that._replaceIds(that.extraSubForm,
                             that.formsetPrefix + '-' + (count-1),
                             that.formsetPrefix + '-' + count);
            that.totalFormsInput.val(count+1);

            that.addButton.parent('.controls').before(newSubForm);
            if(quick) {
                newSubForm.show();
            } else {
                newSubForm.fadeIn();
            }
            that._initSubform(newSubForm);
        },
        _replaceIds: function(el, from, to) {
            $('input,textarea', el).each(function(i,e) {
                var oldName = $(e).attr('name');
                var oldId = $(e).attr('id');
                var name = $(e).attr('name');
                if(name) {
                    $(e).attr('name', oldName.replace(from, to));
                }
                var id = $(e).attr('id');
                if(id) {
                    $(e).attr('id', oldId.replace(from, to));
                }
            });
        }
    });
});
