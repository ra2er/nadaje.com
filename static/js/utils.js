define(['jquery', 'settings', 'lib/jquery.blockUI'], function($, settings) {
    return {
        'filterHtml': function (data, selector) {
            // ripped from jquery internals ;-)
            return $("<div/>").append(data.replace(/<script(.|\s)*?\/script>/g, "")).find(selector);
        },
        'blockElement': function(element, options) {
            // add nice splash on element - tiny wrapper with defaults around blockUI
            if($('.blockUI', element).length === 0) {
                var defaults = {
                    applyPlatformOpacityRules: false,
                    centerX: 0,
                    centerY: 0,
                    css: {
                        backgroundColor: 'transparent',
                        border: 'none',
                        left: '50%',
                        top: '50%',
                        width: 'auto'
                    },
                    message: '<img src="' + settings.THROBBER + '" />',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8
                    }
                };
                $.extend(defaults, options);
                element.block(defaults);
            }
        },
        'parseQuery': function(query) {
            var urlParams;
            var match,
                pl     = /\+/g,  // Regex for replacing addition symbol with a space
                search = /([^&=]+)=?([^&]*)/g,
                decode = function (s) {return decodeURIComponent(s.replace(pl, " "));};

            urlParams = {};
            match = search.exec(query);
            while(match) {
                urlParams[decode(match[1])]=decode(match[2]);
                match = search.exec(query);
            }
            return urlParams;
        }
    };
});

