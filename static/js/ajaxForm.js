require(['jquery', 'utils', 'lib/jquery.ui.widget', 'lib/jquery.form'], function($, utils) {
    $.widget('nadaje.initAjaxForm', {
        options: {
            initForm: function(form) {},
            beforeSubmit: function(form) {},
            complete: function(xhr, textStatus) {},
            success: function(response, widget) {},
            filterForm: function(xhr) {
                return utils.filterHtml(xhr.responseText, 'form');
            }
        },
        _create: function() {
            var that = this;
            that.options.initForm(that.element);
            var data = $(that.element).serialize();
            $(that.element).ajaxForm({
                beforeSubmit: function(arr){
                    $('[type=submit]', that.element).attr('disabled', 'disabled');
                    $('.cancel', that.element).css('opacity', '0.5').click(function(event) {
                        return false;
                    });
                    return that.options.beforeSubmit(that.element);
                },
                complete: function(xhr, textStatus) {
                    that.options.complete(xhr, textStatus);
                    return false;
                },
                success: function(response, textStatus, xhr) {
                    return that.options.success(response, that, xhr);
                },
                error: function(xhr, textStatus) {
                    var form = that.element;
                    if(xhr.status == 400) {
                        var newForm = that.options.filterForm(xhr);
                        if(newForm !== undefined) {
                            form.after(newForm);
                            // _destroy should handle event cleanup
                            form.remove();
                            $(newForm).initAjaxForm(that.options);
                            return false;
                        }
                    }
                    if(xhr.status == 500 && settings.DEBUG) {
                        $(form).html(xhr.responseText);
                        return false;
                    }
                    document.location.reload();
                }
            });
        },
        _destroy: function() {
            that.element.ajaxFormUnbind();
        }
    });
});
