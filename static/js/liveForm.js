define(['jquery', 'settings', 'utils', 'lib/jquery.ui.widget', 'lib/modernizr', 'lib/jquery.deserialize'], function($, settings, utils) {
    $.widget('nadaje.liveForm', {
        options: {
            // submit response is passed to that function
            beforeSubmit: $.noop,
            success: undefined,
            formInputSelector: 'select, input'
        },
        _create: function() {
            var that = this;
            that.useHistory = Modernizr.history && (that.element.attr('method') || 'get').toLowerCase()==='get';
            that.prevState = undefined;
            that.url = that._cleanupUrl(that.element.attr('action') || document.location.href);
            that.timer = undefined;
            if(that.options.success === undefined) {
                throw {
                    message: 'You should provide success function ' +
                             'which extracts submit results content '
                };
            }
            if(that.useHistory) {
                that.initialFormState = that.element.serialize();
                window.onpopstate = function(event) {
                    // I'm not sure if this is feature or a bug in 'deserialize' library
                    // but it doesn't cleanup checkboxes
                    $('[type=checkbox]', that.element).each(function() {
                        $(this).removeAttr("checked");
                    });
                    var state = event.state===null?that.initialFormState:event.state;
                    that.element.deserialize(state, {
                        complete: function() {
                            that._submit(false, event.state!==null);
                        }
                    });
                };
            }
            $(this.options.formInputSelector, that.element).each(function() {
                var input = $(this);
                // checkbox magic behaviour related vars

                if(input.attr('type') === 'checkbox') {
                    that._bindCheckbox(input);
                } else {
                    that._bindField(input);
                }
            });
        },
        _bindField: function(input) {
            var that = this;
            var eventType = input.attr('type')==='text'?'keyup':'change';
            var handlers = {};
            handlers[eventType] = function() {
                if(that.timer !== undefined) {
                    clearTimeout(that.timer);
                }
                that.timer =  setTimeout(function() {
                    that._submit();
                }, 500);
            };
            that._bind(input, handlers);
        },
        _bindCheckbox: function(input) {
            var that = this;
            var name = input.attr('name'), mouseIsDown=false, otherCheckboxes, label, changed=true;
            if(name !== undefined && $('input[type=checkbox][name='+name+']', that.element).length > 1) {
                otherCheckboxes = $('input[type=checkbox][name='+name+']', that.element);
                label = $(input).parent('label');
                that._bind(label, {
                    mousedown: function() {
                        mouseIsDown = true;
                        that.timer = setTimeout(function() {
                            if(mouseIsDown) {
                                otherCheckboxes.removeAttr('checked');
                                input.attr('checked', 'checked');
                                mouseIsDown = false;
                                that._submit();
                            }
                        }, 1000);
                    }
                });
                that._bind(input, {
                    click: function(e) {
                        if(mouseIsDown) {
                            mouseIsDown = false;
                            that._submit();
                            return true;
                        }
                        e.stopPropagation();
                        e.preventDefault();
                        return false;
                    }
                });
            } else {
                that._bindField(input);
            }
        },
        _cleanupUrl: function(url) {
            var that = this;
            var params;
            var parser = document.createElement('a');
            parser.href = url;
            if(parser.search.length > 1) {
                params = utils.parseQuery(parser.search.slice(1));
                $(that.options.formInputSelector, that.element).each(function() {
                    if(params.hasOwnProperty($(this).attr('name'))) {
                        delete params[$(this).attr('name')];
                    }
                });
            }
            var pathname = typeof(parser.pathname)!=='undefined'?parser.pathname:'';
            var query = params?('?'+$.param(params)):'';
            var hash = typeof(parser)!=='undefined'?('#'+parser.hash):'';
            return pathname + query + hash;
        },
        _submit: function(newForm, forceSubmit) {
            newForm = (typeof newForm)==='undefined'?true:newForm;
            forceSubmit = (typeof forceSubmit)==='undefined'?false:forceSubmit;
            var that = this;
            var data = that.element.serialize();
            // with history support we can check whether it is worth to make a request
            //cleanup url from form data
            if(!that.useHistory || forceSubmit || (data !== history.state && (data !== that.initialFormState || that.prevState !== undefined))) {
                that.options.beforeSubmit(that.element);
                $.ajax({
                    data: data,
                    url: that.url,
                    type: that.element.attr('method')===undefined?'get':that.element.attr('method'),
                    success: function(response) {
                        if(data === that.element.serialize()) {
                            that.options.success(response);
                            if(that.useHistory && newForm) {
                                that.prevState = history.state;
                                history.pushState(data, that.element.attr('id'), '?' + data);
                            }
                        }
                    }
                });
            }
        }
    });
});
