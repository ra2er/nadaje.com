define(function() {
    var STATIC_URL = '/static/';
    return {
        'STATIC_URL': STATIC_URL,
        'THROBBER': STATIC_URL + 'img/throbber.gif',
        'MODAL_WIDE': '1000',
        'MODAL_NARROW': '600'
    };
});
