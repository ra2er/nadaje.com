# -*- coding: utf-8 -*-
from email.utils import formataddr
import os

PROJECT_ROOT = os.path.dirname(__file__)

PREFIX_DEFAULT_LOCALE = True
LOCALEURL_USE_ACCEPT_LANGUAGE = False
LOCALEURL_USE_SESSION = True
LOCALE_INDEPENDENT_PATHS = (
    r'^admin/',
)

from localeurl.models import patch_reverse
patch_reverse()

DEBUG = True
TEMPLATE_DEBUG = DEBUG
MAILING_DEBUG = DEBUG

#DOMAIN = 'nadaje.com'
DOMAIN = 'radio.nadaje.com'

MAIL_QUEUE = 'nadaje_mail_queue'

SERVER_EMAIL = 'logging.snc@new.serwery.nadaje.com'
DEFAULT_FROM_EMAIL = formataddr(['nadaje.com', 'info@nadaje.com'])

ADMINS = (
    ('Sylwester Kulpa', 'sylwester.kulpa@gmail.com'),
)

MANAGERS = ADMINS + (
    ('Wojciech Dalętka', 'informacje@nadaje.com'),
)

CONTACT_MAILS = (
    ('nadaje.com', 'informacje@nadaje.com'),
)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'dev.db',
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': ''
    }
}

TIME_ZONE = 'Europe/Warsaw'

LANGUAGE_CODE = 'pl'

_ = lambda s: s

LANGUAGES = (
    ('pl', u'Polski'),
    ('en', u'English'),
)

LOCALE_PATHS = [
    os.path.join(PROJECT_ROOT, 'locale'),
]

SITE_ID = 1

USE_I18N = True

USE_L10N = True

USE_TZ = True

MEDIA_ROOT = os.path.join(PROJECT_ROOT, 'media')

MEDIA_URL = '/media/'

STATIC_ROOT = ''

STATIC_URL = '/static/'

STATICFILES_DIRS = (
    os.path.join(PROJECT_ROOT, 'static'),
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

SECRET_KEY = 'c8*m6zi=h1x+qeboyn-8r$n=o*=_k_0wh-&amp;d^0_#ppj%1o0&amp;3^'

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'localeurl.middleware.LocaleURLMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.transaction.TransactionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'users.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'ajax_middleware.AjaxResposneMiddleware',
    'babel_middleware.LocaleMiddleware',
    'redirect_middleware.RedirectExceptionMiddleware',
)

ROOT_URLCONF = 'urls'

WSGI_APPLICATION = 'wsgi.application'

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.request',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.tz',
    'django.contrib.messages.context_processors.messages',
    'users.context_processors.user',
    'context_processors.language_code',
)

APPS = (
    'admin',
    'users',
    'stations',
    'transmitters',
    'streams',
    'logs'
)

# will be deleted ASAP
DEPRECATED_APPS = (
    'django.contrib.contenttypes',
    'django.contrib.sites',
)

INSTALLED_APPS = (
    'localeurl',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sitemaps',
    'south',
    'utils.images'
) + APPS + DEPRECATED_APPS

REDIS_HOST = '127.0.0.1'
REDIS_PORT = 6379
REDIS_PASSWORD = ''

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

IMAGE_SIZES = {
    'station_logo': {
        'size': (300, 250),
        'crop': False,
        'admin_usable': False
    },
    'station_image_admin': {
        'size': (64, 64),
        'crop': True,
        'admin_usable': True
    },
    'frontend-thumbnail': {
        'size': (64, 64),
        'crop': True,
        'admin_usable': False
    },
    'frontend-thumbnail-small': {
        'size': (32, 32),
        'crop': True,
        'admin_usable': False
    }
}

JINJA2_TEMPLATE_DIRS = [os.path.join(PROJECT_ROOT, 'templates')]

for app_name in APPS:
    if '.' not in app_name:
        templates_dir = os.path.join(PROJECT_ROOT, app_name, 'templates')
        if os.path.exists(templates_dir):
            JINJA2_TEMPLATE_DIRS.append(templates_dir)

AUTHENTICATION_BACKENDS = (
    'users.backends.ModelBackend',
)

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
    }
}

SIGNUP_TOKEN_LIFE = 86400

local_settings = os.path.join(PROJECT_ROOT, 'local_settings.py')
if os.path.exists(local_settings):
    execfile(os.path.join(PROJECT_ROOT, 'local_settings.py'))
