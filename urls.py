import os
from django.conf import settings
from django.conf.urls import patterns, include, url
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.http import Http404, HttpResponsePermanentRedirect, HttpResponseRedirect
from django.shortcuts import redirect, get_object_or_404
from django.utils.translation import ugettext, check_for_language, activate
from urlparse import urlsplit
from localeurl.utils import strip_path, locale_path

import admin.urls
from sitemaps import FlatPageSitemap
from stations.forms import StationSearchForm
import stations.urls
import streams.urls
from stations.models import Genre, Station, RadioStation
import admin.filters
import stations.filters
import transmitters.filters
from transmitters.models import City
import transmitters.urls
import users.urls
from utils.forms import ContactForm
import utils.images.filters
import bootstrap
import templating
from utils.mails import prepare_and_send


def register_filters(env=None):
    bootstrap.register_filters(env)
    utils.images.filters.register_filters(env)
    transmitters.filters.register_filters(env)
    stations.filters.register_filters(env)
    admin.filters.register_filters(env)


def register_globals(env=None):
    transmitters.filters.register_globals(env)

register_filters()
register_globals()

sitemaps = {
    'flatpages': FlatPageSitemap,
}


def contact(request):
    form = ContactForm(data=request.POST or None)
    if form.is_valid():
        extra_context = form.cleaned_data
        prepare_and_send(label='contact', emails=[e for n, e in settings.CONTACT_MAILS], extra_context=extra_context,
                         reply_to=form.cleaned_data['email'])
        messages.success(request, ugettext('Message sent successfully.'))
        return redirect(reverse('home-page'))
    context = {
        'form': form
    }
    return templating.render_to_response('contact.html', request=request, context=context)

def station_redirect(request, slug):
    if slug:
        station = get_object_or_404(Station, slug__iexact=slug).get_subtype_instance()
        if not isinstance(station, RadioStation):
            raise Http404
        redirect_to = reverse('radio-stations:station-details', args=[station.pk])
        return HttpResponsePermanentRedirect(redirect_to)
    else:
        raise Http404()


def city_redirect(request, name):
    city = get_object_or_404(City, name__iexact=name)
    redirect_to = reverse('transmitters:city-details', args=[city.pk])
    return HttpResponsePermanentRedirect(redirect_to)


def set_locale(request, locale=None):
    """
    Ripped from localeurl
    """
    next = request.REQUEST.get('next', None)
    if not next:
        referrer = request.META.get('HTTP_REFERER', None)
        if referrer:
            next = urlsplit(referrer)[2]
    if not next:
        next = '/'
    _, path = strip_path(next)
    if locale and check_for_language(locale):
        if settings.LOCALEURL_USE_SESSION:
            request.session['django_language'] = locale
        path = locale_path(path, locale)
    response = HttpResponseRedirect(path)
    return response


urlpatterns = patterns('',
    url(r'^admin/', include(admin.urls)),
    url(r'^users/', include(users.urls)),
    url(r'^transmitters/', include(transmitters.urls, namespace='transmitters')),
    url(r'^$', lambda request:
        templating.render_to_response('home.html', request=request,
                                      context={'recommended': Station.objects.filter(recommended=True),
                                               'search_form': StationSearchForm(data=request.GET)}),
        name='home-page'),
    url(r'^set-locale/(?P<locale>(pl|en))/', set_locale, name='set-locale'),
    url(r'^stations/', include(stations.urls)),
    url(r'^streams/', include(streams.urls)),
    url(r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': sitemaps}),
    url(r'^jak-sluchac/$', lambda request: HttpResponsePermanentRedirect(reverse('how-to-listen'))),
    url(r'^how-to-listen/$', lambda request: templating.render_to_response('how_to_listen.html', request=request), name='how-to-listen'),
    url(r'^advertisement/$', lambda request: templating.render_to_response('advertisement.html', request=request), name='advertisement'),
    url(r'^services/$', lambda request: templating.render_to_response('services.html', request=request), name='services'),
    url(r'^contact/$', contact, name='contact'),
    url(r'^stacje_zagraniczne.html$', lambda request: HttpResponsePermanentRedirect(reverse('transmitters:foreign-index'))),
    url(r'^rozglosnie_radiowe.html$', lambda request: HttpResponsePermanentRedirect(reverse('radio-stations:station-index', args=['FM']))),
    url(r'^player', lambda request: HttpResponsePermanentRedirect(reverse('home-page'))),
    url(r'^miasta/(?P<name>\w+)\.html$', city_redirect),
    url(r'^(?P<slug>(.*))\.html$', station_redirect),
    url(r'^miasta', lambda request: HttpResponsePermanentRedirect(reverse('transmitters:city-index', args=['FM']))),
    url(r'^rmp', lambda request: HttpResponsePermanentRedirect(reverse('transmitters:map'))),
    url(r'^nadajniki', lambda request: HttpResponsePermanentRedirect(reverse('transmitters:radio-transmitter-index', args=['FM']))),
    url(r'^\bstacje_ze_swiata\b/$', lambda request: HttpResponsePermanentRedirect(reverse('radio-stations:foreign-station-index', args=['FM']))),
    url(r'^\bkatalog_zagraniczny\b', lambda request: HttpResponsePermanentRedirect(reverse('radio-stations:foreign-station-index', args=['INET']))),
)

handler404 = templating.page_not_found
handler500 = templating.server_error
handler403 = templating.forbidden


if settings.DEBUG:
    urlpatterns += patterns('django.views',
        url(r'^media/(.*)', 'static.serve', {
            'document_root': os.path.join(settings.PROJECT_ROOT, 'media'),
        }),
        url(r'^static/(.*)', 'static.serve', {
            'document_root': os.path.join(settings.PROJECT_ROOT, 'static'),
        }),
    )